# XhsEmoticonsKeyboard

**本项目是基于开源项目XhsEmoticonsKeyboard进行ohos化的移植和开发的，可以通过项目标签以及github地址（https://github.com/w446108264/XhsEmoticonsKeyboard ）追踪到原项目版本**

#### 项目介绍

- 项目名称：开源表情键盘

- 所属系列：ohos的第三方组件适配移植

- 功能：

  1. 微信键盘样式

  2. 插入emoji表情集

  3. 插入xhs表情集

  4. 插入微信表情集

  5. 插入我们爱学习表情集

  6. 插入颜文字表情集

  7. 插入测试页集

  8. 展示消息列表，支持发送消息并插入

  9. 组件支持完全自定义,样式支持任意更改

  10. 赠送QQ键盘高仿

- 项目移植状态：完成

- 调用差异：无

- 项目作者和维护人：hihope

- 联系方式：hihope@hoperun.com

- 原项目Doc地址：https://github.com/w446108264/XhsEmoticonsKeyboard

- 原项目基线版本：无

- 编程语言：Java

#### 效果展示
![avatar](screenshot/效果展示.gif)

#### 安装教程

方法1.

1. 下载XhsEmoticonsKeyboard的har包。

2. 启动 DevEco Studio，将下载的har包，导入工程目录“entry->libs”下。

3. 在moudle级别下的build.gradle文件中添加依赖，在dependences标签中增加对libs目录下har包的引用。
```
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
	……
}
```
4. 在导入的har包上点击右键，选择“Add as Library”对包进行引用，选择需要引用的模块，并点击“OK”即引用成功。

方法2.

1. 在工程的build.gradle的allprojects中，添加HAR所在的Maven仓地址
```
repositories {
    maven {
        url 'http://106.15.92.248:8081/repository/Releases/'
    }
}
```
2. 在应用模块的build.gradle的dependencies闭包中，添加如下代码:
```
dependencies {
    implementation 'com.sj.ohos:emoticonkeyboard:1.0.1'
    implementation 'com.sj.ohos:reclib_testemoticons:1.0.1'
    implementation 'com.sj.ohos:reclib_qq:1.0.1'
    implementation 'com.sj.ohos:emoji:1.0.1'
}
```

#### 使用说明

1. 添加布局

   ```
   <?xml version="1.0" encoding="utf-8"?>
   <DependentLayout
       xmlns:ohos="http://schemas.huawei.com/res/ohos"
       ohos:height="match_parent"
       ohos:width="match_parent">
   
       <DirectionalLayout
           ohos:height="match_parent"
           ohos:width="match_parent"
           ohos:orientation="vertical">
   
           <ListContainer
               ohos:id="$+id:lv_chat"
               ohos:height="0vp"
               ohos:width="match_parent"
               ohos:weight="1"/>
   
           <sj.keyboard.XhsEmoticonsKeyBoard
               ohos:id="$+id:ek_bar"
               ohos:height="match_content"
               ohos:width="match_parent"/>
       </DirectionalLayout>
   
   </DependentLayout>
   ```

2. 初始化表情符号键盘栏

   ```
   SimpleCommonUtils.initEmoticonsEditText(ekBar.getEtChat());
   ekBar.setAdapter(SimpleCommonUtils.getCommonAdapter(this, emoticonClickListener));
   ekBar.addFuncView(new SimpleAppsGridView(this));
   
   ekBar.getEtChat().setOnSizeChangedListener((w, h, oldw, oldh) -> scrollToBottom());
   ekBar.getBtnSend().setClickedListener(v -> {
       OnSendBtnClick(ekBar.getEtChat().getText());
       ekBar.getEtChat().setText("");
   });
   ekBar.getEmoticonsToolBarView().addFixedToolItemView(false, ResourceTable.Media_icon_add, null, v ->
           new ToastDialog(SimpleTranslucentChatAbilitySlice.this)
                   .setText("ADD")
                   .setAlignment(LayoutAlignment.CENTER)
                   .setDuration(3000)
                   .show());
   ekBar.getEmoticonsToolBarView().addToolItemView(ResourceTable.Media_icon_setting, v ->
           new ToastDialog(SimpleTranslucentChatAbilitySlice.this)
                   .setText("SETTING")
                   .setAlignment(LayoutAlignment.CENTER)
                   .setDuration(3000)
                    .show());
   ```
   
3. 初始化聊天列表

   ```
   chattingListAdapter = new ChattingListAdapter(this);
           List<ImMsgBean> beanList = new ArrayList<>();
           for (int i = 0; i < 20; i++) {
               ImMsgBean bean = new ImMsgBean();
               bean.setContent("Test:" + i);
               beanList.add(bean);
           }
           chattingListAdapter.addData(beanList);
           lvChat.setItemProvider(chattingListAdapter);
           lvChat.setTouchEventListener(new Component.TouchEventListener() {
               @Override
               public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
                   if (touchEvent.getAction() == TouchEvent.PRIMARY_POINT_DOWN) {
                       ekBar.reset();
                   }
                   return false;
               }
           });
   ```

4. 表情符号键盘点击事件

   ```
   EmoticonClickListener emoticonClickListener = new EmoticonClickListener() {
           @Override
           public void onEmoticonClick(Object o, int actionType, boolean isDelBtn) {
   
               if (isDelBtn) {
                   SimpleCommonUtils.delClick(ekBar.getEtChat());
               } else {
                   if (o == null) {
                       return;
                   }
                   if (actionType == Constants.EMOTICON_CLICK_BIGIMAGE) {
                       if (o instanceof EmoticonEntity) {
                           OnSendImage(((EmoticonEntity) o).getIconUri());
                       }
                   } else {
                       String content = null;
                       if (o instanceof EmojiBean) {
                           content = ((EmojiBean) o).emoji;
                       } else if (o instanceof EmoticonEntity) {
                           content = ((EmoticonEntity) o).getContent();
                       }
   
                       if (content == null || content.isEmpty()) {
                           return;
                       }
                       ekBar.getEtChat().insert(content);
                   }
               }
           }
       };
   ```

5. 插入表情集数据

    ```
    public static PageSetAdapter getCommonAdapter(Context context, EmoticonClickListener emoticonClickListener) {
    
            PageSetAdapter pageSetAdapter = new PageSetAdapter();
    
            addEmojiPageSetEntity(pageSetAdapter, context, emoticonClickListener);
    
            addXhsPageSetEntity(pageSetAdapter, context, emoticonClickListener);
    
            addWechatPageSetEntity(pageSetAdapter, context, emoticonClickListener);
    
            addGoodGoodStudyPageSetEntity(pageSetAdapter, context, emoticonClickListener);
    
            addKaomojiPageSetEntity(pageSetAdapter, context, emoticonClickListener);
    
            addTestPageSetEntity(pageSetAdapter, context);
    
            return pageSetAdapter;
        }
    ```

#### 版本迭代

- v1.0.1

#### 版权和许可信息

- MIT Licence
