package sj.keyboard.data;

import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import sj.keyboard.interfaces.PageViewInstantiateListener;

public class PageEntity<T extends PageEntity> implements PageViewInstantiateListener<T> {

    protected Component mRootView;

    protected PageViewInstantiateListener mPageViewInstantiateListener;

    public void setIPageViewInstantiateItem(PageViewInstantiateListener pageViewInstantiateListener) {
        this.mPageViewInstantiateListener = pageViewInstantiateListener;
    }

    public Component getRootView() {
        return mRootView;
    }

    public void setRootView(Component rootView) {
        this.mRootView = rootView;
    }

    public PageEntity() {
    }

    public PageEntity(Component view) {
        this.mRootView = view;
    }

    @Override
    public Component instantiateItem(ComponentContainer container, int position, T pageEntity) {
        if (mPageViewInstantiateListener != null) {
            return mPageViewInstantiateListener.instantiateItem(container, position, this);
        }
        return getRootView();
    }
}
