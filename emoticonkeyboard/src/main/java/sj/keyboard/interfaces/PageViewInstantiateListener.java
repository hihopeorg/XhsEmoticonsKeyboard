package sj.keyboard.interfaces;

import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import sj.keyboard.data.PageEntity;

public interface PageViewInstantiateListener<T extends PageEntity> {

    Component instantiateItem(ComponentContainer container, int position, T pageEntity);
}
