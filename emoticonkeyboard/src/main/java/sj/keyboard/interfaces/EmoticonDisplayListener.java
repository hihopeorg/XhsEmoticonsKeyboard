package sj.keyboard.interfaces;

import ohos.agp.components.ComponentContainer;
import sj.keyboard.adpater.EmoticonsAdapter;

public interface EmoticonDisplayListener<T> {

    void onBindView(int position, ComponentContainer parent, EmoticonsAdapter.ViewHolder viewHolder, T t, boolean isDelBtn);
}
