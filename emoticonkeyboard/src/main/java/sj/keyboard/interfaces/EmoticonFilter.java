package sj.keyboard.interfaces;

import ohos.agp.components.TextField;
import ohos.agp.components.element.PixelMapElement;
import ohos.app.Context;
import ohos.global.resource.Resource;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;

import java.io.InputStream;

public abstract class EmoticonFilter {

    public abstract void filter(TextField editText, CharSequence text, int start);

    public static PixelMap getDrawableFromAssets(Context context, String emoticonName) {
        try {
            InputStream inputStream = EmoticonFilter.class.getClassLoader().
                    getResourceAsStream(String.format("assets/entry/resources/rawfile/%s", emoticonName));
            PixelMap bitmap = ImageSource.create(inputStream, new ImageSource.SourceOptions()).createPixelmap(new ImageSource.DecodingOptions());
            return bitmap;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static PixelMap getDrawable(Context context, String emojiName) {
        if (emojiName == null || emojiName.isEmpty()) {
            return null;
        }

        if (emojiName.indexOf(".") >= 0) {
            emojiName = emojiName.substring(0, emojiName.indexOf("."));
        }

        try {
            InputStream inputStream = EmoticonFilter.class.getClassLoader().
                    getResourceAsStream(String.format("assets/entry/resources/base/media/%s.png", emojiName));
            PixelMap bitmap = ImageSource.create(inputStream, new ImageSource.SourceOptions()).createPixelmap(new ImageSource.DecodingOptions());
            return bitmap;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static PixelMap getDrawable(Context context, int emoticon) {
        if (emoticon <= 0) {
            return null;
        }
        try {
            Resource resource = context.getResourceManager().getResource(emoticon);
            PixelMap bitmap = new PixelMapElement(resource).getPixelMap();
            return bitmap;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
