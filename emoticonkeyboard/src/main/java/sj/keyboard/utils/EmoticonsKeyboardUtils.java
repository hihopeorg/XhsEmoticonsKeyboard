package sj.keyboard.utils;

import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.agp.components.TextField;
import ohos.agp.render.Paint;
import ohos.agp.window.service.Display;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;
import ohos.global.configuration.DeviceCapability;

import java.lang.reflect.Method;

public class EmoticonsKeyboardUtils {

    private static final int DEF_KEYBOARD_HEAGH_WITH_DP = 280;
    private static int sDefKeyboardHeight = -1;

    public static int getDisplayWidthPixels(Context context) {
        Display display = DisplayManager.getInstance().getDefaultDisplay(context).get();
        return display.getAttributes().width;
    }

    public static int getDisplayHeightPixels(Context context) {
        Display display = DisplayManager.getInstance().getDefaultDisplay(context).get();
        return display.getAttributes().height;
    }

    public static int vp2px(Context context, int vpValue) {
        final int density = context.getResourceManager().getDeviceCapability().screenDensity / DeviceCapability.SCREEN_MDPI;
        return vpValue * density;
    }

    public static int px2vp(Context context, int pxValue) {
        final int density = context.getResourceManager().getDeviceCapability().screenDensity / DeviceCapability.SCREEN_MDPI;
        return pxValue / density;
    }

    public static int getFontHeight(Text textView) {
        Paint paint = new Paint();
        paint.setTextSize(textView.getTextSize());
        Paint.FontMetrics fm = paint.getFontMetrics();
        return (int) Math.ceil(fm.bottom - fm.top);
    }

    public static int getDefKeyboardHeight(Context context) {
        if (sDefKeyboardHeight < 0) {
            sDefKeyboardHeight = vp2px(context, DEF_KEYBOARD_HEAGH_WITH_DP);
        }
        return sDefKeyboardHeight;
    }

    public static boolean isFullScreen(final Context activity) {
        return false;
    }

    public static void openSoftKeyboard(TextField textField) {
        try {
            if (textField != null) {
                textField.setFocusable(Component.FOCUS_ENABLE);
                textField.setFocusBorderEnable(true);
                textField.requestFocus();
            }
            Class inputClass = Class.forName("ohos.miscservices.inputmethod.InputMethodController");
            Method method = inputClass.getMethod("getInstance");
            Object object = method.invoke(new Object[]{});
            Method startInput = inputClass.getMethod("startInput", int.class, boolean.class);
            startInput.invoke(object, 1, true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void closeSoftKeyboard(TextField textField) {
        try {
            if (textField != null) {
                textField.clearFocus();
            }
            Class inputClass = Class.forName("ohos.miscservices.inputmethod.InputMethodController");
            Method method = inputClass.getMethod("getInstance");
            Object object = method.invoke(new Object[]{});
            Method stopInput = inputClass.getMethod("stopInput", int.class);
            stopInput.invoke(object, 1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
