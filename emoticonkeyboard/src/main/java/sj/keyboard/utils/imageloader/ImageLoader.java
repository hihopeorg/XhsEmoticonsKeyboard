package sj.keyboard.utils.imageloader;

import ohos.agp.components.Image;
import ohos.agp.components.element.PixelMapElement;
import ohos.app.Context;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ImageLoader implements ImageBase {

    protected final Context context;

    private volatile static ImageLoader instance;
    private volatile static Pattern NUMBER_PATTERN = Pattern.compile("[0-9]*");

    public static ImageLoader getInstance(Context context) {
        if (instance == null) {
            synchronized (ImageLoader.class) {
                if (instance == null) {
                    instance = new ImageLoader(context);
                }
            }
        }
        return instance;
    }

    public ImageLoader(Context context) {
        this.context = context.getApplicationContext();
    }

    /**
     * @param uriStr
     * @param imageView
     * @throws IOException
     */
    @Override
    public void displayImage(String uriStr, Image imageView) {
        switch (Scheme.ofUri(uriStr)) {
            case FILE:
                displayImageFromFile(uriStr, imageView);
                return;
            case ASSETS:
                displayImageFromAssets(uriStr, imageView);
                return;
            case DRAWABLE:
                displayImageFromDrawable(uriStr, imageView);
                return;
            case HTTP:
            case HTTPS:
                displayImageFromNetwork(uriStr, imageView);
                return;
            case CONTENT:
                displayImageFromContent(uriStr, imageView);
                return;
            case UNKNOWN:
            default:
                Matcher m = NUMBER_PATTERN.matcher(uriStr);
                if (m.matches()) {
                    displayImageFromResource(Integer.parseInt(uriStr), imageView);
                    return;
                }
                displayImageFromOtherSource(uriStr, imageView);
                return;
        }
    }

    /**
     * From File
     *
     * @param imageUri
     * @param imageView
     * @throws IOException
     */
    protected void displayImageFromFile(String imageUri, Image imageView) {
        String filePath = Scheme.FILE.crop(imageUri);
        File file = new File(filePath);
        if (!file.exists()) {
            return;
        }
        PixelMap bitmap;
        try {
            bitmap = ImageSource.create(file, new ImageSource.SourceOptions()).createPixelmap(new ImageSource.DecodingOptions());
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
        if (imageView != null) {
            PixelMapElement mapElement = new PixelMapElement(bitmap);
            imageView.setImageElement(mapElement);
        }
    }

    /**
     * From Assets
     *
     * @param imageUri
     * @param imageView
     * @throws IOException
     */
    protected void displayImageFromAssets(String imageUri, Image imageView) {
        String filePath = Scheme.ASSETS.crop(imageUri);
        PixelMap bitmap;
        try {
            InputStream inputStream = this.getClass().getClassLoader().
                    getResourceAsStream(String.format("assets/entry/resources/rawfile/%s", filePath));
            bitmap = ImageSource.create(inputStream, new ImageSource.SourceOptions()).createPixelmap(new ImageSource.DecodingOptions());
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
        if (imageView != null) {
            imageView.setPixelMap(bitmap);
        }
    }

    /**
     * From Drawable
     *
     * @param imageUri
     * @param imageView
     * @throws IOException
     */
    protected void displayImageFromDrawable(String imageUri, Image imageView) {
        String drawableIdString = Scheme.DRAWABLE.crop(imageUri);
        PixelMap bitmap;
        try {
            InputStream inputStream = this.getClass().getClassLoader().
                    getResourceAsStream(String.format("assets/entry/resources/base/media/%s.png", drawableIdString));
            bitmap = ImageSource.create(inputStream, new ImageSource.SourceOptions()).createPixelmap(new ImageSource.DecodingOptions());
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
        if (imageView != null) {
            imageView.setPixelMap(bitmap);
        }
    }

    /**
     * From Resource
     *
     * @param resID
     * @param imageView
     */
    protected void displayImageFromResource(int resID, Image imageView) {
        if (resID > 0 && imageView != null) {
            imageView.setPixelMap(resID);
        }
    }

    /**
     * From Net
     *
     * @param imageUri
     * @param extra
     * @throws IOException
     */
    protected void displayImageFromNetwork(String imageUri, Object extra) {
    }


    /**
     * From Content
     *
     * @param imageUri
     * @param imageView
     * @throws IOException
     */
    protected void displayImageFromContent(String imageUri, Image imageView) {
    }

    /**
     * From OtherSource
     *
     * @param imageUri
     * @param imageView
     * @throws IOException
     */
    protected void displayImageFromOtherSource(String imageUri, Image imageView) {
    }

}
