package sj.keyboard.widget;

import java.util.ArrayList;
import java.util.List;

import ohos.agp.components.*;
import ohos.app.Context;
import ohos.utils.PlainArray;
import sj.keyboard.utils.EmoticonsKeyboardUtils;

public class FuncLayout extends DirectionalLayout {

    public final int DEF_KEY = Integer.MIN_VALUE;

    private final PlainArray<Component> mFuncViewArrayMap = new PlainArray<>();

    private int mCurrentFuncKey = DEF_KEY;

    protected int mHeight = 0;

    public FuncLayout(Context context, AttrSet attrs) {
        super(context, attrs);
        setOrientation(VERTICAL);
    }

    public void addFuncView(int key, Component view) {
        if (!mFuncViewArrayMap.get(key).toString().contains("Optional.empty")) {
            return;
        }
        mFuncViewArrayMap.put(key, view);
        ComponentContainer.LayoutConfig params = new ComponentContainer.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_PARENT, ComponentContainer.LayoutConfig.MATCH_PARENT);
        addComponent(view, params);
        view.setVisibility(HIDE);
    }

    public void hideAllFuncView() {
        setVisibility(false);
        for (int i = 0; i < mFuncViewArrayMap.size(); i++) {
            int keyTemp = mFuncViewArrayMap.keyAt(i);
            mFuncViewArrayMap.get(keyTemp).get().setVisibility(HIDE);
        }
        mCurrentFuncKey = DEF_KEY;
    }

    public void toggleFuncView(int key, boolean isSoftKeyboardPop, TextField etChat) {
        if (getCurrentFuncKey() == key) {
            if (isSoftKeyboardPop) {
                EmoticonsKeyboardUtils.closeSoftKeyboard(etChat);
            } else {
                hideAllFuncView();
                EmoticonsKeyboardUtils.openSoftKeyboard(etChat);
            }
        } else {
            if (isSoftKeyboardPop) {
                EmoticonsKeyboardUtils.closeSoftKeyboard(etChat);
            }
            getContext().getMainTaskDispatcher().delayDispatch(() -> showFuncView(key), 200);
        }
    }

    public void showFuncView(int key) {
        if (mFuncViewArrayMap.get(key) == null) {
            return;
        }
        for (int i = 0; i < mFuncViewArrayMap.size(); i++) {
            int keyTemp = mFuncViewArrayMap.keyAt(i);
            if (keyTemp == key) {
                mFuncViewArrayMap.get(keyTemp).get().setVisibility(VISIBLE);
            } else {
                mFuncViewArrayMap.get(keyTemp).get().setVisibility(HIDE);
            }
        }
        mCurrentFuncKey = key;
        setVisibility(true);

        if (onFuncChangeListener != null) {
            onFuncChangeListener.onFuncChange(mCurrentFuncKey);
        }
    }

    public int getCurrentFuncKey() {
        return mCurrentFuncKey;
    }

    public void updateHeight(int height) {
        this.mHeight = height;
    }

    public void setVisibility(boolean b) {
        if (b) {
            setVisibility(VISIBLE);
            if (mListenerList != null) {
                for (OnFuncKeyBoardListener l : mListenerList) {
                    l.OnFuncPop(mHeight);
                }
            }
        } else {
            setVisibility(HIDE);
            if (mListenerList != null) {
                for (OnFuncKeyBoardListener l : mListenerList) {
                    l.OnFuncClose();
                }
            }
        }
    }

    private List<OnFuncKeyBoardListener> mListenerList;

    public void addOnKeyBoardListener(OnFuncKeyBoardListener l) {
        if (mListenerList == null) {
            mListenerList = new ArrayList<>();
        }
        mListenerList.add(l);
    }

    public interface OnFuncKeyBoardListener {
        /**
         * 功能布局弹起
         */
        void OnFuncPop(int height);

        /**
         * 功能布局关闭
         */
        void OnFuncClose();
    }

    private OnFuncChangeListener onFuncChangeListener;

    public interface OnFuncChangeListener {
        void onFuncChange(int key);
    }

    public void setOnFuncChangeListener(OnFuncChangeListener listener) {
        this.onFuncChangeListener = listener;
    }
}