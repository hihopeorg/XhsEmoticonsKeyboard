package sj.keyboard.widget;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.TextField;
import ohos.app.Context;
import ohos.multimodalinput.event.KeyEvent;
import sj.keyboard.interfaces.EmoticonFilter;

import java.util.ArrayList;
import java.util.List;

public class EmoticonsEditText extends TextField implements Component.KeyEventListener {

    private List<EmoticonFilter> mFilterList;

    public EmoticonsEditText(Context context, AttrSet attrSet) {
        super(context, attrSet);

        setCursorChangedListener((textField, i, i1) -> {
            if (mFilterList == null) {
                return;
            }
            for (EmoticonFilter emoticonFilter : mFilterList) {
                emoticonFilter.filter(textField, textField.getText(), i);
            }
        });

        setKeyEventListener(this);
    }

    @Override
    public void setTextAlignment(int textAlignment) {
        try {
            super.setTextAlignment(textAlignment);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setText(String text) {
        try {
            super.setText(text);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void addEmoticonFilter(EmoticonFilter emoticonFilter) {
        if (mFilterList == null) {
            mFilterList = new ArrayList<>();
        }
        mFilterList.add(emoticonFilter);
    }

    public void removedEmoticonFilter(EmoticonFilter emoticonFilter) {
        if (mFilterList != null && mFilterList.contains(emoticonFilter)) {
            mFilterList.remove(emoticonFilter);
        }
    }

    @Override
    public boolean onKeyEvent(Component component, KeyEvent keyEvent) {
        if (keyEvent.getKeyCode() == KeyEvent.KEY_BACK) {
            if (onBackKeyClickListener != null) {
                onBackKeyClickListener.onBackKeyClick();
            }
        }
        return false;
    }

    public interface OnBackKeyClickListener {
        void onBackKeyClick();
    }

    OnBackKeyClickListener onBackKeyClickListener;

    public void setOnBackKeyClickListener(OnBackKeyClickListener i) {
        onBackKeyClickListener = i;
    }

    public interface OnSizeChangedListener {
        void onSizeChanged(int w, int h, int oldw, int oldh);
    }

    OnSizeChangedListener onSizeChangedListener;

    public void setOnSizeChangedListener(OnSizeChangedListener i) {
        onSizeChangedListener = i;
    }

}
