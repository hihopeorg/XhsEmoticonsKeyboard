package sj.keyboard.widget;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.DependentLayout;
import ohos.app.Context;
import ohos.global.configuration.Configuration;
import sj.keyboard.utils.IdUtils;

public abstract class AutoHeightLayout extends DependentLayout implements Component.EstimateSizeListener {

    private static final int ID_CHILD = IdUtils.ID_AUTO_LAYOUT;

    protected Context mContext;
    protected int mMaxParentHeight;
    protected boolean mConfigurationChangedFlag = false;

    public AutoHeightLayout(Context context, AttrSet attrs) {
        super(context, attrs);
        this.mContext = context;
        setEstimateSizeListener(this);
    }

    @Override
    public void addComponent(Component child, int index, ComponentContainer.LayoutConfig params) {
        int childSum = getChildCount();
        if (childSum > 1) {
            throw new IllegalStateException("can host only one direct child");
        }
        super.addComponent(child, index, params);
        if (childSum == 0) {
            if (child.getId() < 0) {
                child.setId(ID_CHILD);
            }
            LayoutConfig paramsChild = (LayoutConfig) child.getLayoutConfig();
            paramsChild.addRule(LayoutConfig.ALIGN_PARENT_BOTTOM);
            child.setLayoutConfig(paramsChild);
        } else if (childSum == 1) {
            LayoutConfig paramsChild = (LayoutConfig) child.getLayoutConfig();
            paramsChild.addRule(LayoutConfig.ABOVE, ID_CHILD);
            child.setLayoutConfig(paramsChild);
        }
    }

    public void updateMaxParentHeight(int maxParentHeight) {
        this.mMaxParentHeight = maxParentHeight;
        if (maxParentHeightChangeListener != null) {
            maxParentHeightChangeListener.onMaxParentHeightChange(maxParentHeight);
        }
    }

    @Override
    protected void onAttributeConfigChanged(Configuration config) {
        super.onAttributeConfigChanged(config);
        mConfigurationChangedFlag = true;
    }

    @Override
    public boolean onEstimateSize(int widthMeasureSpec, int heightMeasureSpec) {
        return false;
    }

    public abstract void onSoftKeyboardHeightChanged(int height);

    private OnMaxParentHeightChangeListener maxParentHeightChangeListener;

    public interface OnMaxParentHeightChangeListener {
        void onMaxParentHeightChange(int height);
    }

    public void setOnMaxParentHeightChangeListener(OnMaxParentHeightChangeListener listener) {
        this.maxParentHeightChangeListener = listener;
    }
}