package sj.keyboard.widget;

import java.util.ArrayList;

import com.keyboard.view.ResourceTable;
import ohos.agp.components.AttrSet;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.Image;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.PixelMapElement;
import ohos.app.Context;
import ohos.global.resource.Resource;
import sj.keyboard.data.PageSetEntity;
import sj.keyboard.utils.EmoticonsKeyboardUtils;

public class EmoticonsIndicatorView extends DirectionalLayout {

    private static final int MARGIN_LEFT = 4;
    private static final int INDICATOR_VIEW_SIZE = 20;
    protected Context mContext;
    protected ArrayList<Image> mImageViews;
    protected Element mDrawableSelect;
    protected Element mDrawableNomal;
    protected DirectionalLayout.LayoutConfig mLeftLayoutParams;

    public EmoticonsIndicatorView(Context context, AttrSet attrs) {
        super(context, attrs);
        this.mContext = context;
        this.setOrientation(HORIZONTAL);

        if (attrs.getAttr("bmpSelect").isPresent()) {
            mDrawableSelect = attrs.getAttr("bmpSelect").get().getElement();
        } else {
            Resource resource = null;
            try {
                resource = getResourceManager().getResource(ResourceTable.Media_indicator_point_select);
            } catch (Exception e) {
                e.printStackTrace();
            }
            mDrawableSelect = new PixelMapElement(resource);
        }

        if (attrs.getAttr("bmpNomal").isPresent()) {
            mDrawableNomal = attrs.getAttr("bmpNomal").get().getElement();
        } else {
            Resource resource = null;
            try {
                resource = getResourceManager().getResource(ResourceTable.Media_indicator_point_nomal);
            } catch (Exception e) {
                e.printStackTrace();
            }
            mDrawableNomal = new PixelMapElement(resource);
        }

        mLeftLayoutParams = new DirectionalLayout.LayoutConfig(INDICATOR_VIEW_SIZE, INDICATOR_VIEW_SIZE);
        mLeftLayoutParams.setMarginLeft(EmoticonsKeyboardUtils.vp2px(context, MARGIN_LEFT));
    }

    public void playTo(int position, PageSetEntity pageSetEntity) {
        if (!checkPageSetEntity(pageSetEntity)) {
            return;
        }

        updateIndicatorCount(pageSetEntity.getPageCount());

        for (Image iv : mImageViews) {
            iv.setImageElement(mDrawableNomal);
        }
        mImageViews.get(position).setImageElement(mDrawableSelect);
    }

    public void playBy(int startPosition, int nextPosition, PageSetEntity pageSetEntity) {
        if (!checkPageSetEntity(pageSetEntity)) {
            return;
        }

        updateIndicatorCount(pageSetEntity.getPageCount());

        if (startPosition < 0 || nextPosition < 0 || nextPosition == startPosition) {
            startPosition = nextPosition = 0;
        }

        if (startPosition < 0) {
            startPosition = nextPosition = 0;
        }

        final Image imageViewStrat = mImageViews.get(startPosition);
        final Image imageViewNext = mImageViews.get(nextPosition);

        imageViewStrat.setImageElement(mDrawableNomal);
        imageViewNext.setImageElement(mDrawableSelect);
    }

    protected boolean checkPageSetEntity(PageSetEntity pageSetEntity) {
        if (pageSetEntity != null && pageSetEntity.isShowIndicator()) {
            setVisibility(VISIBLE);
            return true;
        } else {
            setVisibility(HIDE);
            return false;
        }
    }

    protected void updateIndicatorCount(int count) {
        if (mImageViews == null) {
            mImageViews = new ArrayList<>();
        }
        if (count > mImageViews.size()) {
            for (int i = mImageViews.size(); i < count; i++) {
                Image imageView = new Image(mContext);
                imageView.setImageElement(i == 0 ? mDrawableSelect : mDrawableNomal);
                this.addComponent(imageView, mLeftLayoutParams);
                mImageViews.add(imageView);
            }
        }
        for (int i = 0; i < mImageViews.size(); i++) {
            if (i >= count) {
                mImageViews.get(i).setVisibility(HIDE);
            } else {
                mImageViews.get(i).setVisibility(VISIBLE);
            }
        }
    }
}
