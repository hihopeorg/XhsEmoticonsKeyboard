package sj.keyboard.widget;

import com.keyboard.view.ResourceTable;
import ohos.agp.components.*;
import ohos.app.Context;

public class EmoticonPageView extends DependentLayout {

    private ListContainer mGvEmotion;

    public ListContainer getEmoticonsGridView() {
        return mGvEmotion;
    }

    public EmoticonPageView(Context context) {
        this(context, null);
    }

    public EmoticonPageView(Context context, AttrSet attrs) {
        super(context, attrs);
        LayoutScatter layoutScatter = LayoutScatter.getInstance(mContext);
        Component view = layoutScatter.parse(ResourceTable.Layout_item_emoticonpage, this, true);
        mGvEmotion = (ListContainer) view.findComponentById(ResourceTable.Id_gv_emotion);
    }

    public void setNumColumns(int column) {
        TableLayoutManager layoutManager = new TableLayoutManager();
        layoutManager.setColumnCount(column);
        mGvEmotion.setLayoutManager(layoutManager);
    }
}
