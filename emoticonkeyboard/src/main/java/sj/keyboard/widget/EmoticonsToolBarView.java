package sj.keyboard.widget;

import java.util.ArrayList;

import com.keyboard.view.ResourceTable;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.ElementScatter;
import ohos.agp.components.element.ShapeElement;
import ohos.app.Context;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import sj.keyboard.data.PageSetEntity;
import sj.keyboard.utils.IdUtils;
import sj.keyboard.utils.imageloader.ImageLoader;

public class EmoticonsToolBarView extends DependentLayout {

    protected LayoutScatter mLayoutScatter;
    protected Context mContext;
    protected ArrayList<Component> mToolBtnList = new ArrayList<>();
    protected int mBtnWidth;

    protected ScrollView hsv_toolbar;
    protected DirectionalLayout ly_tool;

    private final EventHandler mHandler = new EventHandler(EventRunner.getMainEventRunner());

    public EmoticonsToolBarView(Context context) {
        this(context, null);
    }

    public EmoticonsToolBarView(Context context, AttrSet attrs) {
        super(context, attrs);
        mLayoutScatter = LayoutScatter.getInstance(context);
        mLayoutScatter.parse(ResourceTable.Layout_view_emoticonstoolbar, this, true);
        this.mContext = context;
        try {
            mBtnWidth = (int) getResourceManager().getElement(ResourceTable.Float_bar_tool_btn_width).getFloat();
        } catch (Exception e) {
            e.printStackTrace();
        }
        hsv_toolbar = (ScrollView) findComponentById(ResourceTable.Id_hsv_toolbar);
        ly_tool = (DirectionalLayout) findComponentById(ResourceTable.Id_ly_tool);
    }

    @Override
    public void addComponent(Component child, int index, ComponentContainer.LayoutConfig params) {
        super.addComponent(child, index, params);
        if (getChildCount() > 3) {
            throw new IllegalArgumentException("can host only two direct child");
        }
    }

    public void addFixedToolItemView(Component view, boolean isRight) {
        LayoutConfig params = new LayoutConfig(LayoutConfig.MATCH_CONTENT, LayoutConfig.MATCH_PARENT);
        LayoutConfig hsvParams = (LayoutConfig) hsv_toolbar.getLayoutConfig();
        if (view.getId() <= 0) {
            view.setId(isRight ? IdUtils.IF_TOOLBAR_RIGHT : IdUtils.ID_TOOLBAR_LEFT);
        }
        if (isRight) {
            params.addRule(LayoutConfig.ALIGN_PARENT_RIGHT);
            hsvParams.addRule(LayoutConfig.LEFT_OF, view.getId());
        } else {
            params.addRule(LayoutConfig.ALIGN_PARENT_LEFT);
            hsvParams.addRule(LayoutConfig.RIGHT_OF, view.getId());
        }
        addComponent(view, params);
        hsv_toolbar.setLayoutConfig(hsvParams);
    }

    protected Component getCommonItemToolBtn() {
        return mLayoutScatter == null ? null : mLayoutScatter.parse(ResourceTable.Layout_item_toolbtn, null, false);
    }

    protected void initItemToolBtn(Component toolBtnView, int rec, final PageSetEntity pageSetEntity, ClickedListener onClickListener) {
        Image iv_icon = (Image) toolBtnView.findComponentById(ResourceTable.Id_iv_icon);
        if (rec > 0) {
            iv_icon.setPixelMap(rec);
        }
        DirectionalLayout.LayoutConfig imgParams = new DirectionalLayout.LayoutConfig(mBtnWidth, DirectionalLayout.LayoutConfig.MATCH_CONTENT);
        iv_icon.setLayoutConfig(imgParams);
        if (pageSetEntity != null) {
            iv_icon.setTag(pageSetEntity);
            try {
                ImageLoader.getInstance(mContext).displayImage(pageSetEntity.getIconUri(), iv_icon);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        toolBtnView.setClickedListener(onClickListener != null ? onClickListener : view -> {
            if (mItemClickListeners != null && pageSetEntity != null) {
                mItemClickListeners.onToolBarItemClick(pageSetEntity);
            }
        });
    }

    protected Component getToolBgBtn(Component parentView) {
        return parentView.findComponentById(ResourceTable.Id_iv_icon);
    }

    public void addFixedToolItemView(boolean isRight, int rec, final PageSetEntity pageSetEntity, ClickedListener onClickListener) {
        Component toolBtnView = getCommonItemToolBtn();
        LayoutConfig params = new LayoutConfig(LayoutConfig.MATCH_CONTENT, LayoutConfig.MATCH_PARENT);
        LayoutConfig hsvParams = (LayoutConfig) hsv_toolbar.getLayoutConfig();
        if (toolBtnView.getId() <= 0) {
            toolBtnView.setId(isRight ? IdUtils.IF_TOOLBAR_RIGHT : IdUtils.ID_TOOLBAR_LEFT);
        }
        if (isRight) {
            params.addRule(LayoutConfig.ALIGN_PARENT_RIGHT);
            hsvParams.addRule(LayoutConfig.LEFT_OF, toolBtnView.getId());
        } else {
            params.addRule(LayoutConfig.ALIGN_PARENT_LEFT);
            hsvParams.addRule(LayoutConfig.RIGHT_OF, toolBtnView.getId());
        }
        addComponent(toolBtnView, params);
        hsv_toolbar.setLayoutConfig(hsvParams);
        initItemToolBtn(toolBtnView, rec, pageSetEntity, onClickListener);
    }

    public void addToolItemView(PageSetEntity pageSetEntity) {
        addToolItemView(0, pageSetEntity, null);
    }

    public void addToolItemView(int rec, ClickedListener onClickListener) {
        addToolItemView(rec, null, onClickListener);
    }

    public void addToolItemView(int rec, final PageSetEntity pageSetEntity, ClickedListener onClickListener) {
        Component toolBtnView = getCommonItemToolBtn();
        initItemToolBtn(toolBtnView, rec, pageSetEntity, onClickListener);
        ly_tool.addComponent(toolBtnView);
        mToolBtnList.add(getToolBgBtn(toolBtnView));
    }

    public void setToolBtnSelect(String uuid) {
        if (uuid == null || uuid.isEmpty()) {
            return;
        }
        int select = 0;
        for (int i = 0; i < mToolBtnList.size(); i++) {
            Object object = mToolBtnList.get(i).getTag();
            if (object != null && object instanceof PageSetEntity && uuid.equals(((PageSetEntity) object).getUuid())) {
                int toolbarColor = 0;
                try {
                    toolbarColor = getResourceManager().getElement(ResourceTable.Color_toolbar_btn_select).getColor();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                ShapeElement shapeElement = new ShapeElement();
                shapeElement.setRgbColor(RgbColor.fromArgbInt(toolbarColor));
                mToolBtnList.get(i).setBackground(shapeElement);
                select = i;
            } else {
                Element element = ElementScatter.getInstance(mContext).parse(ResourceTable.Graphic_btn_toolbtn_bg);
                mToolBtnList.get(i).setBackground(element);
            }
        }
        scrollToBtnPosition(select);
    }

    protected void scrollToBtnPosition(final int position) {
        int childCount = ly_tool.getChildCount();
        if (position < childCount) {
            mHandler.postTask(() -> {
                int mScrollX = hsv_toolbar.getScrollValue(AXIS_X);

                int childX = ly_tool.getComponentAt(position).getLeft();

                if (childX < mScrollX) {
                    hsv_toolbar.scrollTo(childX, 0);
                    return;
                }

                int childWidth = ly_tool.getComponentAt(position).getWidth();
                int hsvWidth = hsv_toolbar.getWidth();
                int childRight = childX + childWidth;
                int scrollRight = mScrollX + hsvWidth;

                if (childRight > scrollRight) {
                    hsv_toolbar.scrollTo(childRight - scrollRight, 0);
                }
            });
        }
    }

    public void setBtnWidth(int width) {
        mBtnWidth = width;
    }

    protected OnToolBarItemClickListener mItemClickListeners;

    public interface OnToolBarItemClickListener {
        void onToolBarItemClick(PageSetEntity pageSetEntity);
    }

    public void setOnToolBarItemClickListener(OnToolBarItemClickListener listener) {
        this.mItemClickListeners = listener;
    }
}

