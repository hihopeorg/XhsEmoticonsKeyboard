package sj.keyboard;

import java.util.ArrayList;

import com.keyboard.view.ResourceTable;
import ohos.agp.components.Component;
import ohos.agp.components.LayoutScatter;
import ohos.agp.utils.Color;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.PopupDialog;
import ohos.app.Context;
import sj.keyboard.adpater.PageSetAdapter;
import sj.keyboard.data.PageSetEntity;
import sj.keyboard.utils.EmoticonsKeyboardUtils;
import sj.keyboard.widget.EmoticonsFuncView;
import sj.keyboard.widget.EmoticonsIndicatorView;
import sj.keyboard.widget.EmoticonsToolBarView;

public class EmoticonsKeyBoardPopWindow extends PopupDialog implements EmoticonsFuncView.OnEmoticonsPageViewListener, EmoticonsToolBarView.OnToolBarItemClickListener {

    private Context mContext;
    private EmoticonsFuncView mEmoticonsFuncView;
    private EmoticonsIndicatorView mEmoticonsIndicatorView;
    private EmoticonsToolBarView mEmoticonsToolBarView;

    public EmoticonsKeyBoardPopWindow(Context context) {
        super(context, null);
        this.mContext = context;
        LayoutScatter layoutScatter = LayoutScatter.getInstance(mContext);
        Component component = layoutScatter.parse(ResourceTable.Layout_view_func_emoticon, null, false);
        setCustomComponent(component);
        setSize(EmoticonsKeyboardUtils.getDisplayWidthPixels(mContext), EmoticonsKeyboardUtils.getDefKeyboardHeight(mContext));
        setBackColor(Color.TRANSPARENT);
        updateView(component);
    }

    private void updateView(Component view) {
        mEmoticonsFuncView = ((EmoticonsFuncView) view.findComponentById(ResourceTable.Id_view_epv));
        mEmoticonsIndicatorView = (EmoticonsIndicatorView) view.findComponentById(ResourceTable.Id_view_eiv);
        mEmoticonsToolBarView = (EmoticonsToolBarView) view.findComponentById(ResourceTable.Id_view_etv);
        mEmoticonsFuncView.setOnIndicatorListener(this);
        mEmoticonsToolBarView.setOnToolBarItemClickListener(this);
    }

    public void setAdapter(PageSetAdapter pageSetAdapter) {
        if (pageSetAdapter != null) {
            ArrayList<PageSetEntity> pageSetEntities = pageSetAdapter.getPageSetEntityList();
            if (pageSetEntities != null) {
                for (PageSetEntity pageSetEntity : pageSetEntities) {
                    mEmoticonsToolBarView.addToolItemView(pageSetEntity);
                }
            }
        }
        mEmoticonsFuncView.setAdapter(pageSetAdapter);
    }

    public void showPopupWindow() {
        if (this.isShowing()) {
            this.destroy();
        } else {
            EmoticonsKeyboardUtils.closeSoftKeyboard(null);
            this.showOnCertainPosition(LayoutAlignment.BOTTOM, 0, 200);
        }
    }

    @Override
    public void emoticonSetChanged(PageSetEntity pageSetEntity) {
        mEmoticonsToolBarView.setToolBtnSelect(pageSetEntity.getUuid());
    }

    @Override
    public void playTo(int position, PageSetEntity pageSetEntity) {
        mEmoticonsIndicatorView.playTo(position, pageSetEntity);
    }

    @Override
    public void playBy(int oldPosition, int newPosition, PageSetEntity pageSetEntity) {
        mEmoticonsIndicatorView.playBy(oldPosition, newPosition, pageSetEntity);
    }

    @Override
    public void onToolBarItemClick(PageSetEntity pageSetEntity) {
        mEmoticonsFuncView.setCurrentPageSet(pageSetEntity);
    }

}
