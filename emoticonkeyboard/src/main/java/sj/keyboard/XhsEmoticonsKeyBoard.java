package sj.keyboard;

import java.util.ArrayList;

import com.keyboard.view.ResourceTable;
import sj.keyboard.keyboardvisibilityevent.KeyboardVisibilityEvent;
import ohos.aafwk.ability.AbilitySlice;
import ohos.agp.components.*;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.ElementScatter;
import ohos.app.Context;
import ohos.multimodalinput.event.KeyEvent;
import sj.keyboard.adpater.PageSetAdapter;
import sj.keyboard.data.PageSetEntity;
import sj.keyboard.utils.EmoticonsKeyboardUtils;
import sj.keyboard.widget.AutoHeightLayout;
import sj.keyboard.widget.EmoticonsEditText;
import sj.keyboard.widget.EmoticonsFuncView;
import sj.keyboard.widget.EmoticonsIndicatorView;
import sj.keyboard.widget.EmoticonsToolBarView;
import sj.keyboard.widget.FuncLayout;

public class XhsEmoticonsKeyBoard extends AutoHeightLayout implements Component.ClickedListener, EmoticonsFuncView.OnEmoticonsPageViewListener,
        EmoticonsToolBarView.OnToolBarItemClickListener, EmoticonsEditText.OnBackKeyClickListener, FuncLayout.OnFuncChangeListener, Component.KeyEventListener {

    public static final int FUNC_TYPE_EMOTION = -1;
    public static final int FUNC_TYPE_APPPS = -2;

    protected LayoutScatter mLayoutScatter;

    protected Image mBtnVoiceOrText;
    protected Button mBtnVoice;
    protected EmoticonsEditText mEtChat;
    protected Image mBtnFace;
    protected DependentLayout mRlInput;
    protected Image mBtnMultimedia;
    protected Button mBtnSend;
    protected FuncLayout mLyKvml;

    protected EmoticonsFuncView mEmoticonsFuncView;
    protected EmoticonsIndicatorView mEmoticonsIndicatorView;
    protected EmoticonsToolBarView mEmoticonsToolBarView;

    protected boolean mDispatchKeyEventPreImeLock = false;
    protected boolean mIsSoftKeyboardPop = false;


    public XhsEmoticonsKeyBoard(Context context, AttrSet attrs) {
        super(context, attrs);
        mLayoutScatter = LayoutScatter.getInstance(mContext);
        inflateKeyboardBar();
        initView();
        initFuncView();
        setKeyEventListener(this);
    }

    protected void inflateKeyboardBar() {
        mLayoutScatter.parse(ResourceTable.Layout_view_keyboard_xhs, this, true);
    }

    protected Component inflateFunc() {
        return mLayoutScatter.parse(ResourceTable.Layout_view_func_emoticon, null, false);
    }

    protected void initView() {
        mBtnVoiceOrText = (Image) findComponentById(ResourceTable.Id_btn_voice_or_text);
        mBtnVoice = (Button) findComponentById(ResourceTable.Id_btn_voice);
        mEtChat = (EmoticonsEditText) findComponentById(ResourceTable.Id_et_chat);
        mBtnFace = (Image) findComponentById(ResourceTable.Id_btn_face);
        mRlInput = (DependentLayout) findComponentById(ResourceTable.Id_rl_input);
        mBtnMultimedia = (Image) findComponentById(ResourceTable.Id_btn_multimedia);
        mBtnSend = (Button) findComponentById(ResourceTable.Id_btn_send);
        mLyKvml = (FuncLayout) findComponentById(ResourceTable.Id_ly_kvml);

        mBtnVoiceOrText.setClickedListener(this);
        mBtnFace.setClickedListener(this);
        mBtnMultimedia.setClickedListener(this);
        mEtChat.setOnBackKeyClickListener(this);
    }

    public void setKeyboardVisibilityEvent(AbilitySlice abilitySlice, OnRefreshListListener onRefreshListListener) {
        KeyboardVisibilityEvent keyboardVisibilityEvent = KeyboardVisibilityEvent.getInstance();
        keyboardVisibilityEvent.setAbilitySliceRoot((Component) this.getComponentParent());
        keyboardVisibilityEvent.registerEventListener(abilitySlice, isOpen -> {
            mIsSoftKeyboardPop = isOpen;
            if (isOpen) {
                mLyKvml.hideAllFuncView();
            }
            if (onRefreshListListener != null) {
                onRefreshListListener.OnRefreshList();
            }
        });
    }

    protected void initFuncView() {
        initEmoticonFuncView();
        initEditView();
    }

    protected void initEmoticonFuncView() {
        Component keyboardView = inflateFunc();
        mLyKvml.addFuncView(FUNC_TYPE_EMOTION, keyboardView);
        mEmoticonsFuncView = ((EmoticonsFuncView) keyboardView.findComponentById(ResourceTable.Id_view_epv));

        mEmoticonsIndicatorView = ((EmoticonsIndicatorView) keyboardView.findComponentById(ResourceTable.Id_view_eiv));

        mEmoticonsToolBarView = ((EmoticonsToolBarView) keyboardView.findComponentById(ResourceTable.Id_view_etv));

        mEmoticonsFuncView.setOnIndicatorListener(this);
        mEmoticonsToolBarView.setOnToolBarItemClickListener(this);
        mLyKvml.setOnFuncChangeListener(this);
    }

    protected void initEditView() {

        mEtChat.setTouchEventListener((component, touchEvent) -> {
            if (!mEtChat.isFocused()) {
                mEtChat.setFocusable(Component.FOCUS_ENABLE);
                mEtChat.setTouchFocusable(true);
            }
            return false;
        });

        mEtChat.addTextObserver((s, i, i1, i2) -> {
            if (s != null && !s.isEmpty()) {
                mBtnSend.setVisibility(VISIBLE);
                mBtnMultimedia.setVisibility(HIDE);

                Element element = ElementScatter.getInstance(mContext).parse(ResourceTable.Graphic_btn_send_bg);
                mBtnSend.setBackground(element);

            } else {
                mBtnMultimedia.setVisibility(VISIBLE);
                mBtnSend.setVisibility(HIDE);
            }
        });
    }

    public void setAdapter(PageSetAdapter pageSetAdapter) {
        if (pageSetAdapter != null) {
            ArrayList<PageSetEntity> pageSetEntities = pageSetAdapter.getPageSetEntityList();
            if (pageSetEntities != null) {
                for (PageSetEntity pageSetEntity : pageSetEntities) {
                    mEmoticonsToolBarView.addToolItemView(pageSetEntity);
                }
            }
        }
        mEmoticonsFuncView.setAdapter(pageSetAdapter);
    }

    public void addFuncView(Component view) {
        mLyKvml.addFuncView(FUNC_TYPE_APPPS, view);
    }

    public void reset() {
        mLyKvml.hideAllFuncView();
        mBtnFace.setPixelMap(ResourceTable.Media_icon_face_nomal);
        EmoticonsKeyboardUtils.closeSoftKeyboard(mEtChat);
    }

    protected void showVoice() {
        mRlInput.setVisibility(HIDE);
        mBtnVoice.setVisibility(VISIBLE);
        reset();
    }

    protected void checkVoice() {
        if (mBtnVoice.isComponentDisplayed()) {
            Element element = ElementScatter.getInstance(mContext).parse(ResourceTable.Graphic_btn_voice_or_text_keyboard);
            mBtnVoiceOrText.setImageElement(element);
        } else {
            Element element = ElementScatter.getInstance(mContext).parse(ResourceTable.Graphic_btn_voice_or_text);
            mBtnVoiceOrText.setImageElement(element);
        }
    }

    protected void showText() {
        mRlInput.setVisibility(VISIBLE);
        mBtnVoice.setVisibility(HIDE);
    }

    protected void toggleFuncView(int key) {
        showText();
        mLyKvml.toggleFuncView(key, mIsSoftKeyboardPop, mEtChat);
    }

    @Override
    public void onFuncChange(int key) {
        if (FUNC_TYPE_EMOTION == key) {
            mBtnFace.setPixelMap(ResourceTable.Media_icon_face_pop);
        } else {
            mBtnFace.setPixelMap(ResourceTable.Media_icon_face_nomal);
        }
        checkVoice();
    }

    @Override
    public void onSoftKeyboardHeightChanged(int height) {
        mLyKvml.updateHeight(height);
    }

    public void addOnFuncKeyBoardListener(FuncLayout.OnFuncKeyBoardListener l) {
        mLyKvml.addOnKeyBoardListener(l);
    }

    @Override
    public void emoticonSetChanged(PageSetEntity pageSetEntity) {
        mEmoticonsToolBarView.setToolBtnSelect(pageSetEntity.getUuid());
    }

    @Override
    public void playTo(int position, PageSetEntity pageSetEntity) {
        mEmoticonsIndicatorView.playTo(position, pageSetEntity);
    }

    @Override
    public void playBy(int oldPosition, int newPosition, PageSetEntity pageSetEntity) {
        mEmoticonsIndicatorView.playBy(oldPosition, newPosition, pageSetEntity);
    }

    @Override
    public void onClick(Component v) {
        int i = v.getId();
        if (i == ResourceTable.Id_btn_voice_or_text) {
            if (mRlInput.isComponentDisplayed()) {
                Element element = ElementScatter.getInstance(mContext).parse(ResourceTable.Graphic_btn_voice_or_text_keyboard);
                mBtnVoiceOrText.setBackground(element);
                showVoice();
            } else {
                showText();
                Element element = ElementScatter.getInstance(mContext).parse(ResourceTable.Graphic_btn_voice_or_text);
                mBtnVoiceOrText.setBackground(element);
                mLyKvml.hideAllFuncView();
                EmoticonsKeyboardUtils.openSoftKeyboard(mEtChat);
            }
        } else if (i == ResourceTable.Id_btn_face) {
            toggleFuncView(FUNC_TYPE_EMOTION);
        } else if (i == ResourceTable.Id_btn_multimedia) {
            toggleFuncView(FUNC_TYPE_APPPS);
        }
    }

    @Override
    public void onToolBarItemClick(PageSetEntity pageSetEntity) {
        mEmoticonsFuncView.setCurrentPageSet(pageSetEntity);
    }

    @Override
    public void onBackKeyClick() {
        if (mLyKvml.isComponentDisplayed()) {
            mDispatchKeyEventPreImeLock = true;
            reset();
        }
    }

    @Override
    public boolean onKeyEvent(Component component, KeyEvent event) {
        if (event.getKeyCode() == KeyEvent.KEY_BACK) {
            if (mDispatchKeyEventPreImeLock) {
                mDispatchKeyEventPreImeLock = false;
                return true;
            }
            if (mLyKvml.isComponentDisplayed()) {
                reset();
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    @Override
    public boolean requestFocus() {
        if (EmoticonsKeyboardUtils.isFullScreen(getContext())) {
            return false;
        }
        return super.requestFocus();
    }

    public EmoticonsEditText getEtChat() {
        return mEtChat;
    }

    public Button getBtnVoice() {
        return mBtnVoice;
    }

    public Button getBtnSend() {
        return mBtnSend;
    }

    public EmoticonsFuncView getEmoticonsFuncView() {
        return mEmoticonsFuncView;
    }

    public EmoticonsIndicatorView getEmoticonsIndicatorView() {
        return mEmoticonsIndicatorView;
    }

    public EmoticonsToolBarView getEmoticonsToolBarView() {
        return mEmoticonsToolBarView;
    }

    public interface OnRefreshListListener {
        void OnRefreshList();
    }

}
