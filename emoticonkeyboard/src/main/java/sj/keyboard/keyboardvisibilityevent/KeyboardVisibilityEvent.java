/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sj.keyboard.keyboardvisibilityevent;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.ability.LifecycleObserver;
import ohos.agp.components.Component;
import ohos.agp.utils.Point;
import ohos.agp.utils.Rect;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;

public class KeyboardVisibilityEvent {
    private static final double KEYBOARD_MIN_HEIGHT_RATIO = 0.15;
    private Component mRootComponent;
    private static KeyboardVisibilityEvent keyboardVisibilityEvent;

    public static KeyboardVisibilityEvent getInstance() {
        if (keyboardVisibilityEvent == null) {
            keyboardVisibilityEvent = new KeyboardVisibilityEvent();
        }
        return keyboardVisibilityEvent;
    }

    public void setEventListener(AbilitySlice abilitySlice, KeyboardVisibilityEventListener listener) {
        Unregister unregister = registerEventListener(abilitySlice, listener);
        LifecycleObserver mLifecycleObserver = new LifecycleObserver() {
            @Override
            public void onStop() {
                super.onStop();
                abilitySlice.getLifecycle().removeObserver(this);
                unregister.unregister(mRootComponent);
            }
        };
        abilitySlice.getLifecycle().addObserver(mLifecycleObserver);
    }

    public Unregister registerEventListener(AbilitySlice abilitySlice, KeyboardVisibilityEventListener listener) {
        if (abilitySlice == null) {
            throw new NullPointerException("Parameter:AbilitySlice must not be null");
        }
        if (listener == null) {
            throw new NullPointerException("Parameter:listener must not be null");
        }
        if (mRootComponent == null) {
            throw new NullPointerException("mRootComponent:listener must not be null");
        }
        Component.LayoutRefreshedListener layoutListener = new Component.LayoutRefreshedListener() {
            private boolean wasOpened = false;

            @Override
            public void onRefreshed(Component component) {
                boolean isOpen = isKeyboardVisible(abilitySlice);
                if (isOpen != wasOpened) {
                    wasOpened = isOpen;
                    listener.onVisibilityChanged(isOpen);
                }
            }
        };
        mRootComponent.setLayoutRefreshedListener(layoutListener);
        return new SimpleUnregister(abilitySlice, layoutListener);
    }

    public boolean isKeyboardVisible(AbilitySlice abilitySlice) {
        Rect rect = new Rect();
        mRootComponent.getSelfVisibleRect(rect);
        int[] location = mRootComponent.getLocationOnScreen();
        int screenHeight = (int) getRealHeight(abilitySlice.getContext());
        int heightDiff = screenHeight - rect.getHeight() - location[1];
        if (location[1] == 0) {
            return false;
        } else {
            return heightDiff > screenHeight * KEYBOARD_MIN_HEIGHT_RATIO;
        }
    }

    public void setAbilitySliceRoot(Component rootComponent) {
        this.mRootComponent = rootComponent;
    }

    public static float getRealHeight(Context context) {
        Point point = new Point();
        DisplayManager.getInstance().getDefaultDisplay(context).get().getRealSize(point);
        return point.getPointY();
    }
}
