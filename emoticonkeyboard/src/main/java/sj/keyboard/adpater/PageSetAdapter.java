package sj.keyboard.adpater;

import java.util.ArrayList;

import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.PageSliderProvider;
import sj.keyboard.data.PageEntity;
import sj.keyboard.data.PageSetEntity;

public class PageSetAdapter extends PageSliderProvider {

    private final ArrayList<PageSetEntity> mPageSetEntityList = new ArrayList<>();

    public ArrayList<PageSetEntity> getPageSetEntityList() {
        return mPageSetEntityList;
    }

    public int getPageSetStartPosition(PageSetEntity pageSetEntity) {
        if (pageSetEntity == null || pageSetEntity.getUuid() == null || pageSetEntity.getUuid().isEmpty()) {
            return 0;
        }

        int startPosition = 0;
        for (int i = 0; i < mPageSetEntityList.size(); i++) {
            if (i == mPageSetEntityList.size() - 1 && !pageSetEntity.getUuid().equals(mPageSetEntityList.get(i).getUuid())) {
                return 0;
            }
            if (pageSetEntity.getUuid().equals(mPageSetEntityList.get(i).getUuid())) {
                return startPosition;
            }
            startPosition += mPageSetEntityList.get(i).getPageCount();
        }
        return startPosition;
    }

    public void add(Component view) {
        add(mPageSetEntityList.size(), view);
    }

    public void add(int index, Component view) {
        PageSetEntity pageSetEntity = new PageSetEntity.Builder()
                .addPageEntity(new PageEntity(view))
                .setShowIndicator(false)
                .build();
        mPageSetEntityList.add(index, pageSetEntity);
    }

    public void add(PageSetEntity pageSetEntity) {
        add(mPageSetEntityList.size(), pageSetEntity);
    }

    public void add(int index, PageSetEntity pageSetEntity) {
        if (pageSetEntity == null) {
            return;
        }
        mPageSetEntityList.add(index, pageSetEntity);
    }

    public PageSetEntity get(int position) {
        return mPageSetEntityList.get(position);
    }

    public void remove(int position) {
        mPageSetEntityList.remove(position);
        notifyData();
    }

    public void notifyData() {
    }

    public PageEntity getPageEntity(int position) {
        for (PageSetEntity pageSetEntity : mPageSetEntityList) {
            if (pageSetEntity.getPageCount() > position) {
                return (PageEntity) pageSetEntity.getPageEntityList().get(position);
            } else {
                position -= pageSetEntity.getPageCount();
            }
        }
        return null;
    }

    @Override
    public int getCount() {
        int count = 0;
        for (PageSetEntity pageSetEntity : mPageSetEntityList) {
            count += pageSetEntity.getPageCount();
        }
        return count;
    }

    @Override
    public Object createPageInContainer(ComponentContainer container, int i) {
        Component view = getPageEntity(i).instantiateItem(container, i, null);
        if (view == null) {
            return null;
        }
        container.addComponent(view);
        return view;
    }

    @Override
    public void destroyPageFromContainer(ComponentContainer container, int i, Object o) {
        container.removeComponent((Component) o);
    }

    @Override
    public boolean isPageMatchToObject(Component component, Object o) {
        return component == o;
    }
}
