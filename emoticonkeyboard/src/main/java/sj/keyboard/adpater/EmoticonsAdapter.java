package sj.keyboard.adpater;

import java.util.ArrayList;

import com.keyboard.view.ResourceTable;
import ohos.agp.components.*;
import ohos.app.Context;
import sj.keyboard.data.EmoticonPageEntity;
import sj.keyboard.interfaces.EmoticonClickListener;
import sj.keyboard.interfaces.EmoticonDisplayListener;
import sj.keyboard.utils.EmoticonsKeyboardUtils;

public class EmoticonsAdapter<T> extends BaseItemProvider {

    protected final int DEF_HEIGHTMAXTATIO = 2;
    protected int mDefalutItemHeight = 0;

    protected Context mContext;
    protected LayoutScatter mLayoutScatter;
    protected ArrayList<T> mData = new ArrayList<>();
    protected EmoticonPageEntity mEmoticonPageEntity;
    protected double mItemHeightMaxRatio;
    protected int mItemHeightMax;
    protected int mItemHeightMin;
    protected int mItemHeight;
    protected int mDelBtnPosition;
    protected EmoticonDisplayListener mOnDisPlayListener;
    protected EmoticonClickListener mOnEmoticonClickListener;

    public EmoticonsAdapter(Context context, EmoticonPageEntity emoticonPageEntity,
                            EmoticonClickListener onEmoticonClickListener) {
        this.mContext = context;
        this.mLayoutScatter = LayoutScatter.getInstance(context);
        this.mEmoticonPageEntity = emoticonPageEntity;
        this.mOnEmoticonClickListener = onEmoticonClickListener;
        this.mItemHeightMaxRatio = DEF_HEIGHTMAXTATIO;
        this.mDelBtnPosition = -1;
        try {
            this.mDefalutItemHeight = this.mItemHeight = (int) context.getResourceManager().
                    getElement(ResourceTable.Float_item_emoticon_size_default).getFloat();
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.mData.addAll(emoticonPageEntity.getEmoticonList());
        checkDelBtn(emoticonPageEntity);
    }

    private void checkDelBtn(EmoticonPageEntity entity) {
        EmoticonPageEntity.DelBtnStatus delBtnStatus = entity.getDelBtnStatus();
        if (EmoticonPageEntity.DelBtnStatus.GONE.equals(delBtnStatus)) {
            return;
        }
        if (EmoticonPageEntity.DelBtnStatus.FOLLOW.equals(delBtnStatus)) {
            mDelBtnPosition = getCount();
            mData.add(null);
        } else if (EmoticonPageEntity.DelBtnStatus.LAST.equals(delBtnStatus)) {
            int max = entity.getLine() * entity.getColumn();
            while (getCount() < max) {
                mData.add(null);
            }
            mDelBtnPosition = getCount() - 1;
        }
    }

    @Override
    public int getCount() {
        return mData == null ? 0 : mData.size();
    }

    @Override
    public Object getItem(int position) {
        return mData == null ? null : mData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Component getComponent(int position, Component convertView, ComponentContainer parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = mLayoutScatter.parse(ResourceTable.Layout_item_emoticon, null, false);
            viewHolder.rootView = convertView;
            viewHolder.ly_root = (DirectionalLayout) convertView.findComponentById(ResourceTable.Id_ly_root);
            viewHolder.iv_emoticon = (Image) convertView.findComponentById(ResourceTable.Id_iv_emoticon);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        bindView(position, parent, viewHolder);
        updateUI(viewHolder, parent);
        return convertView;
    }

    protected void bindView(int position, ComponentContainer parent, ViewHolder viewHolder) {
        if (mOnDisPlayListener != null) {
            mOnDisPlayListener.onBindView(position, parent, viewHolder, mData.get(position), position == mDelBtnPosition);
        }
    }

    protected boolean isDelBtn(int position) {
        return position == mDelBtnPosition;
    }

    protected void updateUI(ViewHolder viewHolder, ComponentContainer parent) {
        if (mDefalutItemHeight != mItemHeight) {
            viewHolder.iv_emoticon.setLayoutConfig(new DirectionalLayout.LayoutConfig(DirectionalLayout.LayoutConfig.MATCH_CONTENT, mItemHeight));
        }
        mItemHeightMax = this.mItemHeightMax != 0 ? this.mItemHeightMax : (int) (mItemHeight * mItemHeightMaxRatio);
        mItemHeightMin = this.mItemHeightMin != 0 ? this.mItemHeightMin : mItemHeight;

        int realItemWidth = (EmoticonsKeyboardUtils.getDisplayWidthPixels(mContext) -
                EmoticonsKeyboardUtils.vp2px(mContext, 20)) / mEmoticonPageEntity.getColumn();
        int realItemHeight = EmoticonsKeyboardUtils.getDefKeyboardHeight(mContext) -
                EmoticonsKeyboardUtils.vp2px(mContext, 43) / mEmoticonPageEntity.getLine();

        realItemHeight = Math.min(realItemHeight, mItemHeightMax);
        realItemHeight = Math.max(realItemHeight, mItemHeightMin);

        viewHolder.ly_root.setLayoutConfig(new DirectionalLayout.LayoutConfig(realItemWidth, realItemHeight));
    }

    public void setOnDisPlayListener(EmoticonDisplayListener mOnDisPlayListener) {
        this.mOnDisPlayListener = mOnDisPlayListener;
    }

    public void setItemHeightMaxRatio(double mItemHeightMaxRatio) {
        this.mItemHeightMaxRatio = mItemHeightMaxRatio;
    }

    public void setItemHeightMax(int mItemHeightMax) {
        this.mItemHeightMax = mItemHeightMax;
    }

    public void setItemHeightMin(int mItemHeightMin) {
        this.mItemHeightMin = mItemHeightMin;
    }

    public void setItemHeight(int mItemHeight) {
        this.mItemHeight = mItemHeight;
    }

    public void setDelBtnPosition(int mDelBtnPosition) {
        this.mDelBtnPosition = mDelBtnPosition;
    }

    public static class ViewHolder {
        public Component rootView;
        public DirectionalLayout ly_root;
        public Image iv_emoticon;
    }
}