package sj.qqkeyboard;

import java.util.HashMap;

/**
 * Created by sj on 16/3/16.
 */
public class DefQqEmoticons {

    private static final HashMap<String, Integer> sQqEmoticonHashMap = new HashMap<>();

    static {
        sQqEmoticonHashMap.put("[ecf]", ResourceTable.Media_ecf);
        sQqEmoticonHashMap.put("[ecv]", ResourceTable.Media_ecv);
        sQqEmoticonHashMap.put("[ecb]", ResourceTable.Media_ecb);
        sQqEmoticonHashMap.put("[ecy]", ResourceTable.Media_ecy);
        sQqEmoticonHashMap.put("[ebu]", ResourceTable.Media_ebu);
        sQqEmoticonHashMap.put("[ebr]", ResourceTable.Media_ebr);
        sQqEmoticonHashMap.put("[ecc]", ResourceTable.Media_ecc);
        sQqEmoticonHashMap.put("[eft]", ResourceTable.Media_eft);
        sQqEmoticonHashMap.put("[ecr]", ResourceTable.Media_ecr);
        sQqEmoticonHashMap.put("[ebs]", ResourceTable.Media_ebs);
        sQqEmoticonHashMap.put("[ech]", ResourceTable.Media_ech);
        sQqEmoticonHashMap.put("[ecg]", ResourceTable.Media_ecg);
        sQqEmoticonHashMap.put("[ebh]", ResourceTable.Media_ebh);
        sQqEmoticonHashMap.put("[ebg]", ResourceTable.Media_ebg);
        sQqEmoticonHashMap.put("[ecp]", ResourceTable.Media_ecp);
        sQqEmoticonHashMap.put("[deg]", ResourceTable.Media_deg);
        sQqEmoticonHashMap.put("[ecd]", ResourceTable.Media_ecd);
        sQqEmoticonHashMap.put("[ecj]", ResourceTable.Media_ecj);
        sQqEmoticonHashMap.put("[ebv]", ResourceTable.Media_ebv);
        sQqEmoticonHashMap.put("[ece]", ResourceTable.Media_ece);
        sQqEmoticonHashMap.put("[ebl]", ResourceTable.Media_ebl);
        sQqEmoticonHashMap.put("[eca]", ResourceTable.Media_eca);
        sQqEmoticonHashMap.put("[ecn]", ResourceTable.Media_ecn);
        sQqEmoticonHashMap.put("[eco]", ResourceTable.Media_eco);
        sQqEmoticonHashMap.put("[eeo]", ResourceTable.Media_eeo);
        sQqEmoticonHashMap.put("[eep]", ResourceTable.Media_eep);
        sQqEmoticonHashMap.put("[eci]", ResourceTable.Media_eci);
        sQqEmoticonHashMap.put("[ebj]", ResourceTable.Media_ebj);
        sQqEmoticonHashMap.put("[eer]", ResourceTable.Media_eer);
        sQqEmoticonHashMap.put("[edi]", ResourceTable.Media_edi);
        sQqEmoticonHashMap.put("[ebq]", ResourceTable.Media_ebq);
        sQqEmoticonHashMap.put("[eeq]", ResourceTable.Media_eeq);
        sQqEmoticonHashMap.put("[ecq]", ResourceTable.Media_ecq);
        sQqEmoticonHashMap.put("[ebt]", ResourceTable.Media_ebt);
        sQqEmoticonHashMap.put("[ede]", ResourceTable.Media_ede);
        sQqEmoticonHashMap.put("[eew]", ResourceTable.Media_eew);
        sQqEmoticonHashMap.put("[eex]", ResourceTable.Media_eex);
        sQqEmoticonHashMap.put("[dga]", ResourceTable.Media_dga);
        sQqEmoticonHashMap.put("[ebp]", ResourceTable.Media_ebp);
        sQqEmoticonHashMap.put("[ebo]", ResourceTable.Media_ebo);
    }

    public static HashMap<String, Integer> getQqEmoticonHashMap() {
        return sQqEmoticonHashMap;
    }
}














