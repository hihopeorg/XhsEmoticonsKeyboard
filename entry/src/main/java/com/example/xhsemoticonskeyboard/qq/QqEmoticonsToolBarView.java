package com.example.xhsemoticonskeyboard.qq;

import com.example.xhsemoticonskeyboard.ResourceTable;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.Image;
import ohos.app.Context;
import sj.keyboard.data.PageSetEntity;
import sj.keyboard.utils.imageloader.ImageLoader;
import sj.keyboard.widget.EmoticonsToolBarView;

public class QqEmoticonsToolBarView extends EmoticonsToolBarView {

    public QqEmoticonsToolBarView(Context context) {
        super(context);
    }

    public QqEmoticonsToolBarView(Context context, AttrSet attrs) {
        super(context, attrs);
    }

    protected Component getCommonItemToolBtn() {
        return mLayoutScatter == null ? null : mLayoutScatter.parse(ResourceTable.Layout_item_toolbtn_qq, null, false);
    }

    protected void initItemToolBtn(Component toolBtnView, int rec, final PageSetEntity pageSetEntity, ClickedListener onClickListener) {
        Image iv_icon = (Image) toolBtnView.findComponentById(com.keyboard.view.ResourceTable.Id_iv_icon);
        if (rec > 0) {
            iv_icon.setPixelMap(rec);
        }
        DirectionalLayout.LayoutConfig imgParams = new DirectionalLayout.LayoutConfig(mBtnWidth, LayoutConfig.MATCH_CONTENT);
        iv_icon.setLayoutConfig(imgParams);
        if (pageSetEntity != null) {
            iv_icon.setTag(pageSetEntity);
            try {
                ImageLoader.getInstance(mContext).displayImage(pageSetEntity.getIconUri(), iv_icon);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        toolBtnView.setClickedListener(onClickListener != null ? onClickListener : view -> {
            if (mItemClickListeners != null && pageSetEntity != null) {
                mItemClickListeners.onToolBarItemClick(pageSetEntity);
            }
        });
    }
}

