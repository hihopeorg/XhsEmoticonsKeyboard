package com.example.xhsemoticonskeyboard.qq;

import java.util.ArrayList;

import com.example.xhsemoticonskeyboard.ResourceTable;
import sj.keyboard.keyboardvisibilityevent.KeyboardVisibilityEvent;
import ohos.aafwk.ability.AbilitySlice;
import ohos.agp.components.*;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.ElementScatter;
import ohos.agp.components.element.PixelMapElement;
import ohos.app.Context;
import ohos.global.resource.Resource;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import ohos.multimodalinput.event.KeyEvent;
import sj.keyboard.adpater.PageSetAdapter;
import sj.keyboard.data.PageSetEntity;
import sj.keyboard.utils.EmoticonsKeyboardUtils;
import sj.keyboard.widget.AutoHeightLayout;
import sj.keyboard.widget.EmoticonsEditText;
import sj.keyboard.widget.EmoticonsFuncView;
import sj.keyboard.widget.EmoticonsIndicatorView;
import sj.keyboard.widget.EmoticonsToolBarView;
import sj.keyboard.widget.FuncLayout;

public class QqEmoticonsKeyBoard extends AutoHeightLayout implements EmoticonsFuncView.OnEmoticonsPageViewListener,
        EmoticonsToolBarView.OnToolBarItemClickListener, EmoticonsEditText.OnBackKeyClickListener, FuncLayout.OnFuncChangeListener, Component.KeyEventListener {

    public static final int FUNC_TYPE_PTT = 1;
    public static final int FUNC_TYPE_PTV = 2;
    public static final int FUNC_TYPE_IMAGE = 3;
    public static final int FUNC_TYPE_CAMERA = 4;
    public static final int FUNC_TYPE_HONGBAO = 5;
    public static final int FUNC_TYPE_EMOTICON = 6;
    public static final int FUNC_TYPE_PLUG = 7;

    protected EmoticonsFuncView mEmoticonsFuncView;
    protected EmoticonsIndicatorView mEmoticonsIndicatorView;
    protected QqEmoticonsToolBarView mEmoticonsToolBarView;

    protected boolean mDispatchKeyEventPreImeLock = false;
    protected boolean mIsSoftKeyboardPop = false;

    protected EmoticonsEditText etChat;
    protected Button btnSend;
    protected Image btnVoice;
    protected Image btnPtv;
    protected Image btnImage;
    protected Image btnCamera;
    protected Image btnHongBao;
    protected Image btnEmoticon;
    protected Image btnPlug;
    protected FuncLayout lyKvml;

    public QqEmoticonsKeyBoard(Context context, AttrSet attrs) {
        super(context, attrs);
        LayoutScatter.getInstance(context).parse(ResourceTable.Layout_view_keyboard_qq, this, true);
        initView();
        initFuncView();
        setKeyEventListener(this);
    }

    protected void initView() {
        etChat = (EmoticonsEditText) findComponentById(ResourceTable.Id_et_chat);
        btnSend = (Button) findComponentById(ResourceTable.Id_btn_send);
        btnVoice = (Image) findComponentById(ResourceTable.Id_btn_voice);
        btnPtv = (Image) findComponentById(ResourceTable.Id_btn_ptv);
        btnImage = (Image) findComponentById(ResourceTable.Id_btn_image);
        btnCamera = (Image) findComponentById(ResourceTable.Id_btn_camera);
        btnHongBao = (Image) findComponentById(ResourceTable.Id_btn_hongbao);
        btnEmoticon = (Image) findComponentById(ResourceTable.Id_btn_emoticon);
        btnPlug = (Image) findComponentById(ResourceTable.Id_btn_plug);
        lyKvml = (FuncLayout) findComponentById(ResourceTable.Id_ly_kvml);


        etChat.setOnBackKeyClickListener(this);
        btnVoice.setClickedListener(component -> {
            toggleFuncView(FUNC_TYPE_PTT);
        });
        btnPtv.setClickedListener(component -> {
            toggleFuncView(FUNC_TYPE_PTV);
        });
        btnImage.setClickedListener(component -> {
        });
        btnCamera.setClickedListener(component -> {
        });
        btnHongBao.setClickedListener(component -> {
        });
        btnEmoticon.setClickedListener(component -> {
            toggleFuncView(FUNC_TYPE_EMOTICON);
        });
        btnPlug.setClickedListener(component -> {
            toggleFuncView(FUNC_TYPE_PLUG);
        });
    }

    public void setKeyboardVisibilityEvent(AbilitySlice abilitySlice, OnRefreshListListener onRefreshListListener) {
        KeyboardVisibilityEvent keyboardVisibilityEvent = KeyboardVisibilityEvent.getInstance();
        keyboardVisibilityEvent.setAbilitySliceRoot((Component) this.getComponentParent());
        keyboardVisibilityEvent.registerEventListener(abilitySlice, isOpen -> {
            mIsSoftKeyboardPop = isOpen;
            if (isOpen) {
                lyKvml.hideAllFuncView();
            }
            if (onRefreshListListener != null) {
                onRefreshListListener.OnRefreshList();
            }
        });
    }

    protected void initFuncView() {
        initEmoticonFuncView();
        initEditView();
    }

    protected void initEmoticonFuncView() {
        Component keyboardView = inflateFunc();
        lyKvml.addFuncView(FUNC_TYPE_EMOTICON, keyboardView);
        mEmoticonsFuncView = ((EmoticonsFuncView) findComponentById(com.keyboard.view.ResourceTable.Id_view_epv));
        mEmoticonsIndicatorView = ((EmoticonsIndicatorView) findComponentById(com.keyboard.view.ResourceTable.Id_view_eiv));
        mEmoticonsToolBarView = ((QqEmoticonsToolBarView) findComponentById(com.keyboard.view.ResourceTable.Id_view_etv));
        mEmoticonsFuncView.setOnIndicatorListener(this);
        mEmoticonsToolBarView.setOnToolBarItemClickListener(this);
        lyKvml.setOnFuncChangeListener(this);
    }

    protected Component inflateFunc() {
        return LayoutScatter.getInstance(getContext()).parse(ResourceTable.Layout_view_func_emoticon_qq, null, false);
    }

    protected void initEditView() {
        etChat.setTouchEventListener((v, event) -> {
            if (!etChat.isFocused()) {
                etChat.setFocusable(Component.FOCUS_ENABLE);
                etChat.setTouchFocusable(true);
            }
            return false;
        });

        etChat.addTextObserver((s, i, i1, i2) -> {
            Element element;
            if (s != null && !s.isEmpty()) {
                element = ElementScatter.getInstance(mContext).parse(com.keyboard.view.ResourceTable.Graphic_btn_send_bg);
            } else {
                element = ElementScatter.getInstance(mContext).parse(com.keyboard.view.ResourceTable.Graphic_btn_send_bg_disable);
            }
            btnSend.setBackground(element);
        });
    }

    public void setAdapter(PageSetAdapter pageSetAdapter) {
        if (pageSetAdapter != null) {
            ArrayList<PageSetEntity> pageSetEntities = pageSetAdapter.getPageSetEntityList();
            if (pageSetEntities != null) {
                for (PageSetEntity pageSetEntity : pageSetEntities) {
                    mEmoticonsToolBarView.addToolItemView(pageSetEntity);
                }
            }
        }
        mEmoticonsFuncView.setAdapter(pageSetAdapter);
    }

    public void addFuncView(int type, Component view) {
        lyKvml.addFuncView(type, view);
    }

    public void reset() {
        resetIcon();
        lyKvml.hideAllFuncView();
        EmoticonsKeyboardUtils.closeSoftKeyboard(etChat);
    }

    public void resetIcon() {
        btnVoice.setBackground(graphicToElement(ResourceTable.Graphic_qq_skin_aio_panel_ptt));
        btnPtv.setBackground(graphicToElement(ResourceTable.Graphic_qq_skin_aio_panel_ptv));
        btnImage.setBackground(graphicToElement(ResourceTable.Graphic_qq_skin_aio_panel_image));
        btnCamera.setBackground(graphicToElement(ResourceTable.Graphic_qq_skin_aio_panel_camera));
        btnHongBao.setBackground(graphicToElement(ResourceTable.Graphic_qq_skin_aio_panel_hongbao));
        btnEmoticon.setBackground(graphicToElement(ResourceTable.Graphic_qq_skin_aio_panel_emotion));
        btnPlug.setBackground(graphicToElement(ResourceTable.Graphic_qq_skin_aio_panel_plus));
    }

    public Element graphicToElement(int resId) {
        return ElementScatter.getInstance(mContext).parse(resId);
    }

    public Element mediaToElement(int resId) {
        Resource resource = null;
        try {
            resource = getResourceManager().getResource(resId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        ImageSource imageSource = ImageSource.create(resource, new ImageSource.SourceOptions());
        PixelMap pixelMap = imageSource.createPixelmap(new ImageSource.DecodingOptions());
        return new PixelMapElement(pixelMap);
    }

    protected void toggleFuncView(int key) {
        lyKvml.toggleFuncView(key, mIsSoftKeyboardPop, etChat);
    }

    @Override
    public void onFuncChange(int key) {
        resetIcon();
        switch (key) {
            case FUNC_TYPE_PTT:
                btnVoice.setBackground(mediaToElement(ResourceTable.Media_qq_skin_aio_panel_ptt_press));
                break;
            case FUNC_TYPE_PTV:
                btnPtv.setBackground(mediaToElement(ResourceTable.Media_qq_skin_aio_panel_ptv_press));
                break;
            case FUNC_TYPE_IMAGE:
                btnImage.setBackground(mediaToElement(ResourceTable.Media_qq_skin_aio_panel_image_press));
                break;
            case FUNC_TYPE_CAMERA:
                btnCamera.setBackground(mediaToElement(ResourceTable.Media_qq_skin_aio_panel_camera_press));
                break;
            case FUNC_TYPE_HONGBAO:
                btnHongBao.setBackground(mediaToElement(ResourceTable.Media_qq_skin_aio_panel_hongbao_press));
                break;
            case FUNC_TYPE_EMOTICON:
                btnEmoticon.setBackground(mediaToElement(ResourceTable.Media_qq_skin_aio_panel_emotion_press));
                break;
            case FUNC_TYPE_PLUG:
                btnPlug.setBackground(mediaToElement(ResourceTable.Media_qq_skin_aio_panel_plus_press));
                break;
        }
    }

    @Override
    public void onSoftKeyboardHeightChanged(int height) {
        lyKvml.updateHeight(height);
    }

    public void addOnFuncKeyBoardListener(FuncLayout.OnFuncKeyBoardListener l) {
        lyKvml.addOnKeyBoardListener(l);
    }

    @Override
    public void emoticonSetChanged(PageSetEntity pageSetEntity) {
        mEmoticonsToolBarView.setToolBtnSelect(pageSetEntity.getUuid());
    }

    @Override
    public void playTo(int position, PageSetEntity pageSetEntity) {
        mEmoticonsIndicatorView.playTo(position, pageSetEntity);
    }

    @Override
    public void playBy(int oldPosition, int newPosition, PageSetEntity pageSetEntity) {
        mEmoticonsIndicatorView.playBy(oldPosition, newPosition, pageSetEntity);
    }

    @Override
    public void onToolBarItemClick(PageSetEntity pageSetEntity) {
        mEmoticonsFuncView.setCurrentPageSet(pageSetEntity);
    }

    @Override
    public void onBackKeyClick() {
        if (lyKvml.isComponentDisplayed()) {
            mDispatchKeyEventPreImeLock = true;
            reset();
        }
    }


    @Override
    public boolean onKeyEvent(Component component, KeyEvent event) {
        switch (event.getKeyCode()) {
            case KeyEvent.KEY_BACK:
                if (mDispatchKeyEventPreImeLock) {
                    mDispatchKeyEventPreImeLock = false;
                    return true;
                }
                if (lyKvml.isComponentDisplayed()) {
                    reset();
                    return true;
                } else {
                    return false;
                }
        }
        return false;
    }

    public EmoticonsEditText getEtChat() {
        return etChat;
    }

    public EmoticonsFuncView getEmoticonsFuncView() {
        return mEmoticonsFuncView;
    }

    public EmoticonsIndicatorView getEmoticonsIndicatorView() {
        return mEmoticonsIndicatorView;
    }

    public EmoticonsToolBarView getEmoticonsToolBarView() {
        return mEmoticonsToolBarView;
    }

    public Button getBtnSend() {
        return btnSend;
    }

    public interface OnRefreshListListener {
        void OnRefreshList();
    }
}
