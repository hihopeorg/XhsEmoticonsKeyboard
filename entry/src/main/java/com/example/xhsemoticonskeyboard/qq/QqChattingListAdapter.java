package com.example.xhsemoticonskeyboard.qq;

import com.example.xhsemoticonskeyboard.common.adapter.ChattingListAdapter;
import ohos.aafwk.ability.AbilitySlice;
import ohos.agp.components.Text;

public class QqChattingListAdapter extends ChattingListAdapter {

    public QqChattingListAdapter(AbilitySlice activity) {
        super(activity);
    }

    public void setContent(Text tv_content, String content) {
        QqUtils.spannableEmoticonFilter(tv_content, content);
    }
}