package com.example.xhsemoticonskeyboard.qq;

import com.example.xhsemoticonskeyboard.common.Constants;
import com.example.xhsemoticonskeyboard.common.SimpleCommonUtils;
import com.example.xhsemoticonskeyboard.common.filter.QqFilter;
import com.example.xhsemoticonskeyboard.common.utils.ParseDataUtils;
import com.sj.emoji.EmojiBean;
import ohos.agp.components.Text;
import ohos.agp.components.TextField;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.ElementScatter;
import ohos.app.Context;
import sj.keyboard.adpater.EmoticonsAdapter;
import sj.keyboard.adpater.PageSetAdapter;
import sj.keyboard.data.EmoticonEntity;
import sj.keyboard.data.EmoticonPageEntity;
import sj.keyboard.data.EmoticonPageSetEntity;
import sj.keyboard.data.PageEntity;
import sj.keyboard.data.PageSetEntity;
import sj.keyboard.interfaces.EmoticonClickListener;
import sj.keyboard.interfaces.EmoticonDisplayListener;
import sj.keyboard.interfaces.PageViewInstantiateListener;
import sj.keyboard.utils.EmoticonsKeyboardUtils;
import sj.keyboard.utils.imageloader.ImageBase;
import sj.keyboard.utils.imageloader.ImageLoader;
import sj.keyboard.widget.EmoticonPageView;
import sj.keyboard.widget.EmoticonsEditText;
import sj.qqkeyboard.DefQqEmoticons;
import sj.qqkeyboard.ResourceTable;

public class QqUtils extends SimpleCommonUtils {

    public static void initEmoticonsEditText(EmoticonsEditText etContent) {
        etContent.addEmoticonFilter(new QqFilter());
    }

    public static EmoticonClickListener getCommonEmoticonClickListener(final TextField editText) {
        return new EmoticonClickListener() {
            @Override
            public void onEmoticonClick(Object o, int actionType, boolean isDelBtn) {
                if (isDelBtn) {
                    QqUtils.delClick(editText);
                } else {
                    if (o == null) {
                        return;
                    }
                    if (actionType == Constants.EMOTICON_CLICK_TEXT) {
                        String content = null;
                        if (o instanceof EmojiBean) {
                            content = ((EmojiBean) o).emoji;
                        } else if (o instanceof EmoticonEntity) {
                            content = ((EmoticonEntity) o).getContent();
                        }

                        if (content == null || content.isEmpty()) {
                            return;
                        }

                        editText.insert(content);
                    }
                }
            }
        };
    }

    public static PageSetAdapter getCommonAdapter(Context context, EmoticonClickListener emoticonClickListener) {

        PageSetAdapter pageSetAdapter = new PageSetAdapter();

        addQqPageSetEntity(pageSetAdapter, context, emoticonClickListener);

        PageSetEntity pageSetEntity1 = new PageSetEntity.Builder()
                .addPageEntity(new PageEntity(new SimpleQqGridView(context)))
                .setIconUri(ResourceTable.Media_dec)
                .setShowIndicator(false)
                .build();

        pageSetAdapter.add(pageSetEntity1);

        PageSetEntity pageSetEntity2 = new PageSetEntity.Builder()
                .addPageEntity(new PageEntity(new SimpleQqGridView(context)))
                .setIconUri(ResourceTable.Media_mwi)
                .setShowIndicator(false)
                .build();

        pageSetAdapter.add(pageSetEntity2);

        return pageSetAdapter;
    }

    public static void addQqPageSetEntity(PageSetAdapter pageSetAdapter, Context context, final EmoticonClickListener emoticonClickListener) {
        EmoticonPageSetEntity kaomojiPageSetEntity = new EmoticonPageSetEntity.Builder()
                .setLine(3)
                .setColumn(7)
                .setEmoticonList(ParseDataUtils.ParseQqData(DefQqEmoticons.getQqEmoticonHashMap()))
                .setIPageViewInstantiateItem((PageViewInstantiateListener<EmoticonPageEntity>) (container, position, pageEntity) -> {
                    if (pageEntity.getRootView() == null) {
                        EmoticonPageView pageView = new EmoticonPageView(container.getContext());
                        pageView.setNumColumns(pageEntity.getColumn());
                        pageEntity.setRootView(pageView);
                        try {
                            EmoticonsAdapter adapter = new EmoticonsAdapter(container.getContext(), pageEntity, emoticonClickListener);
                            adapter.setItemHeightMaxRatio(1.8);
                            adapter.setOnDisPlayListener(getEmoticonDisplayListener(emoticonClickListener));
                            pageView.getEmoticonsGridView().setItemProvider(adapter);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    return pageEntity.getRootView();
                })
                .setShowDelBtn(EmoticonPageEntity.DelBtnStatus.LAST)
                .setIconUri(ImageBase.Scheme.DRAWABLE.toUri("kys"))
                .build();
        pageSetAdapter.add(kaomojiPageSetEntity);
    }

    public static EmoticonDisplayListener<Object> getEmoticonDisplayListener(final EmoticonClickListener emoticonClickListener) {
        return (position, parent, viewHolder, object, isDelBtn) -> {
            final EmoticonEntity emoticonEntity = (EmoticonEntity) object;
            if (emoticonEntity == null && !isDelBtn) {
                return;
            }
            Element element = ElementScatter.getInstance(parent.getContext()).parse(com.keyboard.view.ResourceTable.Graphic_bg_emoticon);
            viewHolder.ly_root.setBackground(element);

            if (isDelBtn) {
                viewHolder.iv_emoticon.setPixelMap(com.example.xhsemoticonskeyboard.ResourceTable.Media_icon_del);
            } else {
                try {
                    ImageLoader.getInstance(viewHolder.iv_emoticon.getContext()).displayImage(emoticonEntity.getIconUri(), viewHolder.iv_emoticon);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            viewHolder.rootView.setClickedListener(v -> {
                if (emoticonClickListener != null) {
                    emoticonClickListener.onEmoticonClick(emoticonEntity, Constants.EMOTICON_CLICK_TEXT, isDelBtn);
                }
            });
        };
    }

    public static void spannableEmoticonFilter(Text tv_content, String content) {

        StringBuilder spannableStringBuilder = new StringBuilder(content);
        String spannable = QqFilter.spannableFilter(tv_content.getContext(),
                spannableStringBuilder.toString(),
                content,
                EmoticonsKeyboardUtils.getFontHeight(tv_content),
                null);
        tv_content.setText(spannable);

        //todo Span相关
//        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(content);
//        Spannable spannable = QqFilter.spannableFilter(tv_content.getContext(),
//                spannableStringBuilder,
//                content,
//                EmoticonsKeyboardUtils.getFontHeight(tv_content),
//                null);
//        tv_content.setText(spannable);
    }
}
