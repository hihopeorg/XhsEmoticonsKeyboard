package com.example.xhsemoticonskeyboard.qq;

import com.example.xhsemoticonskeyboard.ResourceTable;
import com.example.xhsemoticonskeyboard.common.adapter.AppsAdapter;
import com.example.xhsemoticonskeyboard.common.data.AppBean;

import java.util.ArrayList;

import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.app.Context;
import sj.keyboard.utils.EmoticonsKeyboardUtils;

public class SimpleQqGridView extends DependentLayout {

    protected Component view;
    protected Context context;

    public SimpleQqGridView(Context context) {
        this(context, null);
    }

    public SimpleQqGridView(Context context, AttrSet attrs) {
        super(context, attrs);
        this.context = context;
        LayoutScatter layoutScatter = LayoutScatter.getInstance(context);
        view = layoutScatter.parse(ResourceTable.Layout_view_apps, this, true);

        int viewColor = 0;
        try {
            viewColor = getResourceManager().getElement(com.keyboard.view.ResourceTable.Color_white).getColor();
        } catch (Exception e) {
            e.printStackTrace();
        }
        ShapeElement shapeElement = new ShapeElement();
        shapeElement.setRgbColor(RgbColor.fromArgbInt(viewColor));
        setBackground(shapeElement);

        init();
    }

    protected void init() {
        ListContainer gv_apps = (ListContainer) view.findComponentById(ResourceTable.Id_gv_apps);

        gv_apps.setPadding(0, 0, 0, EmoticonsKeyboardUtils.vp2px(context, 20));
        DirectionalLayout.LayoutConfig params = (DirectionalLayout.LayoutConfig) gv_apps.getLayoutConfig();
        params.setMarginBottom(EmoticonsKeyboardUtils.vp2px(context, 2));
        gv_apps.setLayoutConfig(params);

        TableLayoutManager tableLayoutManager = new TableLayoutManager();
        tableLayoutManager.setColumnCount(4);
        gv_apps.setLayoutManager(tableLayoutManager);

        ArrayList<AppBean> mAppBeanList = new ArrayList<>();
        mAppBeanList.add(new AppBean(ResourceTable.Media_lcw, "QQ电话"));
        mAppBeanList.add(new AppBean(ResourceTable.Media_lde, "视频电话"));
        mAppBeanList.add(new AppBean(ResourceTable.Media_ldh, "短视频"));
        mAppBeanList.add(new AppBean(ResourceTable.Media_lcx, "收藏"));
        mAppBeanList.add(new AppBean(ResourceTable.Media_ldc, "发红包"));
        mAppBeanList.add(new AppBean(ResourceTable.Media_ldk, "转账"));
        mAppBeanList.add(new AppBean(ResourceTable.Media_ldf, "悄悄话"));
        mAppBeanList.add(new AppBean(ResourceTable.Media_lcu, "文件"));
        AppsAdapter adapter = new AppsAdapter(getContext(), mAppBeanList);
        gv_apps.setItemProvider(adapter);
    }
}
