package com.example.xhsemoticonskeyboard;

import com.example.xhsemoticonskeyboard.slice.SimpleChatOnCoordinatorLayoutAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class SimpleChatOnCoordinatorLayoutAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(SimpleChatOnCoordinatorLayoutAbilitySlice.class.getName());
    }
}
