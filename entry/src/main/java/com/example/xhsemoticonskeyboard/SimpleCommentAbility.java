package com.example.xhsemoticonskeyboard;

import com.example.xhsemoticonskeyboard.slice.SimpleCommentAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class SimpleCommentAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(SimpleCommentAbilitySlice.class.getName());
    }
}
