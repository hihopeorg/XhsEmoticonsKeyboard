package com.example.xhsemoticonskeyboard;

import com.example.xhsemoticonskeyboard.slice.QqAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class QqAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(QqAbilitySlice.class.getName());
    }
}
