package com.example.xhsemoticonskeyboard.userdef;

import com.example.xhsemoticonskeyboard.ResourceTable;
import com.example.xhsemoticonskeyboard.common.data.AppBean;
import com.example.xhsemoticonskeyboard.common.widget.SimpleAppsGridView;
import ohos.agp.components.AttrSet;
import ohos.agp.components.ListContainer;
import ohos.agp.components.TableLayoutManager;
import ohos.app.Context;

import java.util.ArrayList;

public class SimpleUserDefAppsGridView extends SimpleAppsGridView {

    public SimpleUserDefAppsGridView(Context context) {
        super(context);
    }

    public SimpleUserDefAppsGridView(Context context, AttrSet attrs) {
        super(context, attrs);
    }

    protected void init() {
        ListContainer gv_apps = (ListContainer) view.findComponentById(ResourceTable.Id_gv_apps);
        TableLayoutManager tableLayoutManager = new TableLayoutManager();
        tableLayoutManager.setColumnCount(2);
        gv_apps.setLayoutManager(tableLayoutManager);

        ArrayList<AppBean> mAppBeanList = new ArrayList<>();
        mAppBeanList.add(new AppBean(ResourceTable.Media_chatting_photo, "图片"));
        mAppBeanList.add(new AppBean(ResourceTable.Media_chatting_camera, "拍照"));
        ChattingAppsAdapter adapter = new ChattingAppsAdapter(getContext(), mAppBeanList);
        gv_apps.setItemProvider(adapter);
    }
}
