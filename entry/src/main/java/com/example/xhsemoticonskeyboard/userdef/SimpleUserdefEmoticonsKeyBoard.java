package com.example.xhsemoticonskeyboard.userdef;

import com.example.xhsemoticonskeyboard.ResourceTable;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.app.Context;
import sj.keyboard.XhsEmoticonsKeyBoard;
import sj.keyboard.utils.EmoticonsKeyboardUtils;

public class SimpleUserdefEmoticonsKeyBoard extends XhsEmoticonsKeyBoard {

    public SimpleUserdefEmoticonsKeyBoard(Context context, AttrSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void inflateKeyboardBar() {
        mLayoutScatter.parse(ResourceTable.Layout_view_keyboard_userdef, this, true);
    }

    @Override
    protected Component inflateFunc() {
        return mLayoutScatter.parse(ResourceTable.Layout_view_func_emoticon_userdef, null, false);
    }

    @Override
    public void reset() {
        mLyKvml.hideAllFuncView();
        mBtnFace.setPixelMap(ResourceTable.Media_chatting_emoticons);
        EmoticonsKeyboardUtils.closeSoftKeyboard(mEtChat);
    }

    @Override
    public void onFuncChange(int key) {
        if (FUNC_TYPE_EMOTION == key) {
            mBtnFace.setPixelMap(ResourceTable.Media_chatting_softkeyboard);
        } else {
            mBtnFace.setPixelMap(ResourceTable.Media_chatting_emoticons);
        }
        checkVoice();
    }

    @Override
    protected void showText() {
        mRlInput.setVisibility(VISIBLE);
        mBtnVoice.setVisibility(HIDE);
    }

    @Override
    protected void showVoice() {
        mRlInput.setVisibility(HIDE);
        mBtnVoice.setVisibility(VISIBLE);
        reset();
    }

    @Override
    protected void checkVoice() {
        if (mBtnVoice.isComponentDisplayed()) {
            mBtnVoiceOrText.setPixelMap(ResourceTable.Media_chatting_softkeyboard);
        } else {
            mBtnVoiceOrText.setPixelMap(ResourceTable.Media_chatting_vodie);
        }
    }

    @Override
    public void onClick(Component v) {
        int i = v.getId();
        if (i == com.keyboard.view.ResourceTable.Id_btn_voice_or_text) {
            if (mRlInput.isComponentDisplayed()) {
                mBtnVoiceOrText.setPixelMap(ResourceTable.Media_chatting_softkeyboard);
                showVoice();
            } else {
                showText();
                mBtnVoiceOrText.setPixelMap(ResourceTable.Media_chatting_vodie);
                mLyKvml.hideAllFuncView();
                EmoticonsKeyboardUtils.openSoftKeyboard(mEtChat);
            }
        } else if (i == com.keyboard.view.ResourceTable.Id_btn_face) {
            toggleFuncView(FUNC_TYPE_EMOTION);
        } else if (i == com.keyboard.view.ResourceTable.Id_btn_multimedia) {
            toggleFuncView(FUNC_TYPE_APPPS);
        }
    }
}
