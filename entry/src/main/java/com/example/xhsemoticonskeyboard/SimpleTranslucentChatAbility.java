package com.example.xhsemoticonskeyboard;

import com.example.xhsemoticonskeyboard.slice.SimpleTranslucentChatAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class SimpleTranslucentChatAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(SimpleTranslucentChatAbilitySlice.class.getName());
    }
}
