package com.example.xhsemoticonskeyboard;

import com.example.xhsemoticonskeyboard.slice.SimpleChatUserDefAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class SimpleChatUserDefAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(SimpleChatUserDefAbilitySlice.class.getName());
    }
}
