package com.example.xhsemoticonskeyboard;

import com.example.xhsemoticonskeyboard.slice.SimpleChatAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class SimpleChatAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(SimpleChatAbilitySlice.class.getName());
    }
}
