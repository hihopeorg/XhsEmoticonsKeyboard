package com.example.xhsemoticonskeyboard.common.widget;

import com.example.xhsemoticonskeyboard.ResourceTable;
import com.example.xhsemoticonskeyboard.common.adapter.AppsAdapter;
import com.example.xhsemoticonskeyboard.common.data.AppBean;
import ohos.agp.components.*;
import ohos.app.Context;

import java.util.ArrayList;

public class SimpleAppsGridView extends DependentLayout {

    protected Component view;

    public SimpleAppsGridView(Context context) {
        this(context, null);
    }

    public SimpleAppsGridView(Context context, AttrSet attrs) {
        super(context, attrs);
        LayoutScatter layoutScatter = LayoutScatter.getInstance(context);
        view = layoutScatter.parse(ResourceTable.Layout_view_apps, this, true);
        init();
    }

    protected void init() {
        ListContainer gv_apps = (ListContainer) view.findComponentById(ResourceTable.Id_gv_apps);
        TableLayoutManager tableLayoutManager =   new TableLayoutManager();
        tableLayoutManager.setColumnCount(4);
        gv_apps.setLayoutManager(tableLayoutManager);

        ArrayList<AppBean> mAppBeanList = new ArrayList<>();
        mAppBeanList.add(new AppBean(ResourceTable.Media_icon_photo, "图片"));
        mAppBeanList.add(new AppBean(ResourceTable.Media_icon_camera, "拍照"));
        mAppBeanList.add(new AppBean(ResourceTable.Media_icon_audio, "视频"));
        mAppBeanList.add(new AppBean(ResourceTable.Media_icon_qzone, "空间"));
        mAppBeanList.add(new AppBean(ResourceTable.Media_icon_contact, "联系人"));
        mAppBeanList.add(new AppBean(ResourceTable.Media_icon_file, "文件"));
        mAppBeanList.add(new AppBean(ResourceTable.Media_icon_loaction, "位置"));
        AppsAdapter adapter = new AppsAdapter(getContext(), mAppBeanList);
        gv_apps.setItemProvider(adapter);
    }
}
