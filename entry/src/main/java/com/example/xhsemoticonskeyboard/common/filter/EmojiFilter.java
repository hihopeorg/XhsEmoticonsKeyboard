package com.example.xhsemoticonskeyboard.common.filter;

import com.sj.emoji.EmojiDisplay;

import java.util.regex.Matcher;

import ohos.agp.components.TextField;
import sj.keyboard.interfaces.EmoticonFilter;
import sj.keyboard.utils.EmoticonsKeyboardUtils;

public class EmojiFilter extends EmoticonFilter {

    private int emojiSize = -1;

    @Override
    public void filter(TextField editText, CharSequence text, int start) {
        emojiSize = emojiSize == -1 ? EmoticonsKeyboardUtils.getFontHeight(editText) : emojiSize;
        clearSpan(editText.getText(), start, text.toString().length());
        Matcher m = EmojiDisplay.getMatcher(text.toString().substring(start, text.toString().length()));
        if (m != null) {
            while (m.find()) {
                String emojiHex = Integer.toHexString(Character.codePointAt(m.group(), 0));
                EmojiDisplay.emojiDisplay(editText.getContext(), editText.getText(), emojiHex, emojiSize, start + m.start(), start + m.end());
            }
        }
    }

    //todo Span相关
    private void clearSpan(String spannable, int start, int end) {
        if (start == end) {
            return;
        }
//        EmojiSpan[] oldSpans = spannable.getSpans(start, end, EmojiSpan.class);
//        for (int i = 0; i < oldSpans.length; i++) {
//            spannable.removeSpan(oldSpans[i]);
//        }
    }

//    private void clearSpan(Spannable spannable, int start, int end) {
//        if (start == end) {
//            return;
//        }
//        EmojiSpan[] oldSpans = spannable.getSpans(start, end, EmojiSpan.class);
//        for (int i = 0; i < oldSpans.length; i++) {
//            spannable.removeSpan(oldSpans[i]);
//        }
//    }
}
