package com.example.xhsemoticonskeyboard.common.adapter.emoticonadapter;

import com.example.xhsemoticonskeyboard.ResourceTable;
import com.example.xhsemoticonskeyboard.common.Constants;

import ohos.agp.components.*;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.ElementScatter;
import ohos.app.Context;
import sj.keyboard.adpater.EmoticonsAdapter;
import sj.keyboard.data.EmoticonEntity;
import sj.keyboard.data.EmoticonPageEntity;
import sj.keyboard.interfaces.EmoticonClickListener;
import sj.keyboard.utils.EmoticonsKeyboardUtils;
import sj.keyboard.utils.imageloader.ImageLoader;

public class BigEmoticonsAdapter extends EmoticonsAdapter<EmoticonEntity> {

    protected final double DEF_HEIGHTMAXTATIO = 1.6;

    public BigEmoticonsAdapter(Context context, EmoticonPageEntity emoticonPageEntity, EmoticonClickListener onEmoticonClickListener) {
        super(context, emoticonPageEntity, onEmoticonClickListener);
        try {
            this.mItemHeight = (int) context.getResourceManager().getElement(ResourceTable.Float_item_emoticon_size_big).getFloat();
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.mItemHeightMaxRatio = DEF_HEIGHTMAXTATIO;
    }

    @Override
    public Component getComponent(int position, Component convertView, ComponentContainer parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = mLayoutScatter.parse(ResourceTable.Layout_item_emoticon_big, null, false);
            viewHolder.rootView = convertView;
            viewHolder.ly_root = (DirectionalLayout) convertView.findComponentById(ResourceTable.Id_ly_root);
            viewHolder.iv_emoticon = (Image) convertView.findComponentById(ResourceTable.Id_iv_emoticon);
            viewHolder.tv_content = (Text) convertView.findComponentById(ResourceTable.Id_tv_content);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        bindView(position, viewHolder);
        updateUI(viewHolder, parent);
        return convertView;
    }

    protected void updateUI(ViewHolder viewHolder, ComponentContainer parent) {
        if (mDefalutItemHeight != mItemHeight) {
            viewHolder.iv_emoticon.setLayoutConfig(new DirectionalLayout.LayoutConfig(DirectionalLayout.LayoutConfig.MATCH_CONTENT, mItemHeight));
        }
        mItemHeightMax = this.mItemHeightMax != 0 ? this.mItemHeightMax : (int) (mItemHeight * mItemHeightMaxRatio);
        mItemHeightMin = this.mItemHeightMin != 0 ? this.mItemHeightMin : mItemHeight;
        int realItemWidth = (EmoticonsKeyboardUtils.getDisplayWidthPixels(mContext) -
                EmoticonsKeyboardUtils.vp2px(mContext, 20)) / mEmoticonPageEntity.getColumn();
        int realItemHeight = EmoticonsKeyboardUtils.getDefKeyboardHeight(mContext) -
                EmoticonsKeyboardUtils.vp2px(mContext, 43) / mEmoticonPageEntity.getLine();

        realItemHeight = Math.min(realItemHeight, mItemHeightMax);
        realItemHeight = Math.max(realItemHeight, mItemHeightMin);
        viewHolder.ly_root.setLayoutConfig(new DirectionalLayout.LayoutConfig(realItemWidth, realItemHeight));
    }

    protected void bindView(int position, ViewHolder viewHolder) {
        final boolean isDelBtn = isDelBtn(position);
        final EmoticonEntity emoticonEntity = mData.get(position);
        if (isDelBtn) {
            viewHolder.iv_emoticon.setPixelMap(ResourceTable.Media_icon_del);

            Element element = ElementScatter.getInstance(mContext).parse(com.keyboard.view.ResourceTable.Graphic_bg_emoticon);
            viewHolder.iv_emoticon.setBackground(element);
        } else {
            if (emoticonEntity != null) {
                try {
                    ImageLoader.getInstance(viewHolder.iv_emoticon.getContext()).displayImage(emoticonEntity.getIconUri(), viewHolder.iv_emoticon);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                Element element = ElementScatter.getInstance(mContext).parse(com.keyboard.view.ResourceTable.Graphic_bg_emoticon);
                viewHolder.iv_emoticon.setBackground(element);
            }
        }

        viewHolder.rootView.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component v) {
                if (mOnEmoticonClickListener != null) {
                    mOnEmoticonClickListener.onEmoticonClick(emoticonEntity, Constants.EMOTICON_CLICK_BIGIMAGE, isDelBtn);
                }
            }
        });
    }

    public static class ViewHolder {
        public Component rootView;
        public DirectionalLayout ly_root;
        public Image iv_emoticon;
        public Text tv_content;
    }
}