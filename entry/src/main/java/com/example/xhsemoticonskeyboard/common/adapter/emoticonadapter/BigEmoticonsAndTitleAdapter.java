package com.example.xhsemoticonskeyboard.common.adapter.emoticonadapter;

import com.example.xhsemoticonskeyboard.ResourceTable;
import com.example.xhsemoticonskeyboard.common.Constants;

import ohos.agp.components.Component;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.ElementScatter;
import ohos.app.Context;
import sj.keyboard.data.EmoticonEntity;
import sj.keyboard.data.EmoticonPageEntity;
import sj.keyboard.interfaces.EmoticonClickListener;
import sj.keyboard.utils.imageloader.ImageLoader;

public class BigEmoticonsAndTitleAdapter extends BigEmoticonsAdapter {

    protected final double DEF_HEIGHTMAXTATIO = 1.6;

    public BigEmoticonsAndTitleAdapter(Context context, EmoticonPageEntity emoticonPageEntity, EmoticonClickListener onEmoticonClickListener) {
        super(context, emoticonPageEntity, onEmoticonClickListener);
        try {
            this.mItemHeight = (int) context.getResourceManager().getElement(ResourceTable.Float_item_emoticon_size_big).getFloat();
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.mItemHeightMaxRatio = DEF_HEIGHTMAXTATIO;
    }

    protected void bindView(int position, ViewHolder viewHolder) {
        final boolean isDelBtn = isDelBtn(position);
        final EmoticonEntity emoticonEntity = mData.get(position);
        if (isDelBtn) {
            viewHolder.iv_emoticon.setPixelMap(ResourceTable.Media_icon_del);

            Element element = ElementScatter.getInstance(mContext).parse(com.keyboard.view.ResourceTable.Graphic_bg_emoticon);
            viewHolder.iv_emoticon.setBackground(element);
        } else {
            if (emoticonEntity != null) {
                try {
                    ImageLoader.getInstance(viewHolder.iv_emoticon.getContext()).displayImage(emoticonEntity.getIconUri(), viewHolder.iv_emoticon);
                    viewHolder.tv_content.setVisibility(Component.VISIBLE);
                    viewHolder.tv_content.setText(emoticonEntity.getContent());
                } catch (Exception e) {
                    e.printStackTrace();
                }

                Element element = ElementScatter.getInstance(mContext).parse(com.keyboard.view.ResourceTable.Graphic_bg_emoticon);
                viewHolder.iv_emoticon.setBackground(element);
            }
        }

        viewHolder.rootView.setClickedListener(v -> {
            if (mOnEmoticonClickListener != null) {
                mOnEmoticonClickListener.onEmoticonClick(emoticonEntity, Constants.EMOTICON_CLICK_BIGIMAGE, isDelBtn);
            }
        });
    }
}