package com.example.xhsemoticonskeyboard.common.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import ohos.app.Context;
import ohos.javax.xml.stream.XMLInputFactory;
import ohos.javax.xml.stream.XMLStreamException;
import ohos.javax.xml.stream.XMLStreamReader;
import sj.keyboard.data.EmoticonEntity;
import sj.keyboard.data.EmoticonPageEntity;
import sj.keyboard.data.EmoticonPageSetEntity;

public class XmlUtil {

    Context mContext;

    public XmlUtil(Context context) {
        this.mContext = context;
    }

    public InputStream getXmlFromAssets(String xmlName) {
        try {
            InputStream inStream = XmlUtil.class.getClassLoader().
                    getResourceAsStream(String.format("assets/entry/resources/rawfile/%s", xmlName));
            return inStream;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public InputStream getXmlFromSD(String filePath) {
        try {
            File file = new File(filePath);
            if (file.exists()) {
                FileInputStream inStream = new FileInputStream(file);
                return inStream;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public EmoticonPageSetEntity<EmoticonEntity> ParserXml(String filePath, InputStream inStream) {

        EmoticonPageSetEntity.Builder<EmoticonEntity> emoticonPageSetEntity = new EmoticonPageSetEntity.Builder<>();
        ArrayList<EmoticonEntity> emoticonList = new ArrayList<>();
        emoticonPageSetEntity.setEmoticonList(emoticonList);
        EmoticonEntity emoticonBeanTemp = null;
        String skeyName = "";
        boolean isFirstIconUri = true;
        int event = 0;

        if (null != inStream) {

            try {
                XMLStreamReader pullParser = XMLInputFactory.newInstance().createXMLStreamReader(inStream);

                while (pullParser.hasNext()) {
                    switch (event) {
                        case XMLStreamReader.START_ELEMENT:
                            skeyName = pullParser.getName().toString();
                            break;
                        case XMLStreamReader.CHARACTERS:
                            String charactersText = pullParser.getText().trim();

                            if (skeyName.equals("name") && !charactersText.isEmpty()) {
                                emoticonPageSetEntity.setSetName(charactersText);
                            } else if (skeyName.equals("line") && !charactersText.isEmpty()) {
                                emoticonPageSetEntity.setLine(Integer.parseInt(charactersText));
                            } else if (skeyName.equals("row") && !charactersText.isEmpty()) {
                                emoticonPageSetEntity.setColumn(Integer.parseInt(charactersText));
                            } else if (skeyName.equals("iconUri") && !charactersText.isEmpty() && isFirstIconUri) {
                                emoticonPageSetEntity.setIconUri(charactersText);
                                isFirstIconUri = false;
                            } else if (skeyName.equals("isShowDelBtn") && charactersText.isEmpty()) {
                                emoticonPageSetEntity.setShowDelBtn(EmoticonPageEntity.DelBtnStatus.GONE);
                            } else if (skeyName.equals("eventType") && !charactersText.isEmpty()) {
                                emoticonBeanTemp = new EmoticonEntity();
                                emoticonBeanTemp.setEventType(Long.parseLong(charactersText));
                            } else if (skeyName.equals("iconUri") && !charactersText.isEmpty()) {
                                emoticonBeanTemp.setIconUri("file://" + filePath + "/" + charactersText);
                            } else if (skeyName.equals("content") && !charactersText.isEmpty()) {
                                emoticonBeanTemp.setContent(charactersText);
                                emoticonList.add(emoticonBeanTemp);
                            }
                            break;
                        case XMLStreamReader.END_ELEMENT:
                            break;
                        default:
                            break;
                    }
                    event = pullParser.next();
                }
                return new EmoticonPageSetEntity(emoticonPageSetEntity);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return new EmoticonPageSetEntity(emoticonPageSetEntity);
    }
}
