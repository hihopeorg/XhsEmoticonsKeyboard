package com.example.xhsemoticonskeyboard.common.adapter.emoticonadapter;

import com.example.xhsemoticonskeyboard.ResourceTable;
import com.example.xhsemoticonskeyboard.common.Constants;

import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.Text;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.ElementScatter;
import ohos.app.Context;
import sj.keyboard.adpater.EmoticonsAdapter;
import sj.keyboard.data.EmoticonEntity;
import sj.keyboard.data.EmoticonPageEntity;
import sj.keyboard.interfaces.EmoticonClickListener;
import sj.keyboard.utils.EmoticonsKeyboardUtils;

public class TextEmoticonsAdapter extends EmoticonsAdapter<EmoticonEntity> {

    public TextEmoticonsAdapter(Context context, EmoticonPageEntity emoticonPageEntity, EmoticonClickListener onEmoticonClickListener) {
        super(context, emoticonPageEntity, onEmoticonClickListener);
    }

    @Override
    public Component getComponent(int position, Component convertView, ComponentContainer parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = mLayoutScatter.parse(ResourceTable.Layout_item_emoticon_text, null, false);
            viewHolder.rootView = convertView;
            viewHolder.ly_root = (DirectionalLayout) convertView.findComponentById(ResourceTable.Id_ly_root);
            viewHolder.tv_content = (Text) convertView.findComponentById(ResourceTable.Id_tv_content);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        final boolean isDelBtn = isDelBtn(position);
        final EmoticonEntity emoticonEntity = mData.get(position);
        if (isDelBtn) {
            Element element = ElementScatter.getInstance(mContext).parse(com.keyboard.view.ResourceTable.Graphic_bg_emoticon);
            viewHolder.ly_root.setBackground(element);
        } else {
            viewHolder.tv_content.setVisibility(Component.VISIBLE);
            if (emoticonEntity != null) {
                viewHolder.tv_content.setText(emoticonEntity.getContent());

                Element element = ElementScatter.getInstance(mContext).parse(com.keyboard.view.ResourceTable.Graphic_bg_emoticon);
                viewHolder.ly_root.setBackground(element);
            }
        }

        viewHolder.rootView.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component v) {
                if (mOnEmoticonClickListener != null) {
                    mOnEmoticonClickListener.onEmoticonClick(emoticonEntity, Constants.EMOTICON_CLICK_TEXT, isDelBtn);
                }
            }
        });

        updateUI(viewHolder, parent);
        return convertView;
    }

    protected void updateUI(ViewHolder viewHolder, ComponentContainer parent) {
        if (mDefalutItemHeight != mItemHeight) {
            viewHolder.tv_content.setLayoutConfig(new DirectionalLayout.LayoutConfig(DirectionalLayout.LayoutConfig.MATCH_CONTENT, mItemHeight));
        }
        mItemHeightMax = this.mItemHeightMax != 0 ? this.mItemHeightMax : (int) (mItemHeight * mItemHeightMaxRatio);
        mItemHeightMin = this.mItemHeightMin != 0 ? this.mItemHeightMin : mItemHeight;

        int realItemWidth = (EmoticonsKeyboardUtils.getDisplayWidthPixels(mContext) -
                EmoticonsKeyboardUtils.vp2px(mContext, 20)) / mEmoticonPageEntity.getColumn();
        int realItemHeight = EmoticonsKeyboardUtils.getDefKeyboardHeight(mContext) -
                EmoticonsKeyboardUtils.vp2px(mContext, 43) / mEmoticonPageEntity.getLine();

        realItemHeight = Math.min(realItemHeight, mItemHeightMax);
        realItemHeight = Math.max(realItemHeight, mItemHeightMin);

        viewHolder.ly_root.setLayoutConfig(new DirectionalLayout.LayoutConfig(realItemWidth, realItemHeight));
    }

    public static class ViewHolder {
        public Component rootView;
        public DirectionalLayout ly_root;
        public Text tv_content;
    }
}