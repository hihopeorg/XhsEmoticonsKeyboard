package com.example.xhsemoticonskeyboard.common.adapter;

import com.example.xhsemoticonskeyboard.ResourceTable;
import com.example.xhsemoticonskeyboard.common.SimpleCommonUtils;
import com.example.xhsemoticonskeyboard.common.data.ImMsgBean;

import java.util.ArrayList;
import java.util.List;

import ohos.aafwk.ability.AbilitySlice;
import ohos.agp.components.*;
import sj.keyboard.utils.imageloader.ImageLoader;

public class ChattingListAdapter extends BaseItemProvider {

    private final int VIEW_TYPE_COUNT = 2;
    private final int VIEW_TYPE_LEFT_TEXT = 0;
    private final int VIEW_TYPE_LEFT_IMAGE = 1;

    private AbilitySlice mActivity;
    private LayoutScatter mLayoutScatter;
    private List<ImMsgBean> mData;

    public ChattingListAdapter(AbilitySlice activity) {
        this.mActivity = activity;
        mLayoutScatter = LayoutScatter.getInstance(activity);
    }

    public void addData(List<ImMsgBean> data) {
        if (data == null || data.size() == 0) {
            return;
        }
        if (mData == null) {
            mData = new ArrayList<>();
        }
        for (ImMsgBean bean : data) {
            addData(bean, false, false);
        }
        this.notifyDataChanged();
    }

    public void addData(ImMsgBean bean, boolean isNotifyDataSetChanged, boolean isFromHead) {
        if (bean == null) {
            return;
        }
        if (mData == null) {
            mData = new ArrayList<>();
        }

        if (bean.getMsgType() <= 0) {
            String content = bean.getContent();
            if (content != null) {
                if (content.indexOf("[img]") >= 0) {
                    bean.setImage(content.replace("[img]", ""));
                    bean.setMsgType(ImMsgBean.CHAT_MSGTYPE_IMG);
                } else {
                    bean.setMsgType(ImMsgBean.CHAT_MSGTYPE_TEXT);
                }
            }
        }

        if (isFromHead) {
            mData.add(0, bean);
        } else {
            mData.add(bean);
        }

        if (isNotifyDataSetChanged) {
            this.notifyDataChanged();
        }
    }

    @Override
    public int getCount() {
        return mData == null ? 0 : mData.size();
    }

    @Override
    public Object getItem(int position) {
        return mData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemComponentType(int position) {
        if (mData.get(position) == null) {
            return -1;
        }
        return mData.get(position).getMsgType() == ImMsgBean.CHAT_MSGTYPE_TEXT ? VIEW_TYPE_LEFT_TEXT : VIEW_TYPE_LEFT_IMAGE;
    }

    @Override
    public int getComponentTypeCount() {
        return VIEW_TYPE_COUNT;
    }

    @Override
    public Component getComponent(int position, Component convertView, ComponentContainer parent) {
        final ImMsgBean bean = mData.get(position);
        int type = getItemComponentType(position);
        Component holderView = null;
        switch (type) {
            case VIEW_TYPE_LEFT_TEXT:
                ViewHolderLeftText holder;
                if (convertView == null) {
                    holder = new ViewHolderLeftText();
                    holderView = mLayoutScatter.parse(ResourceTable.Layout_listitem_cha_left_text, null, false);
                    holderView.setFocusable(Component.FOCUS_ENABLE);
                    holder.iv_avatar = (Image) holderView.findComponentById(ResourceTable.Id_iv_avatar);
                    holder.tv_content = (Text) holderView.findComponentById(ResourceTable.Id_tv_content);
                    holderView.setTag(holder);
                    convertView = holderView;
                } else {
                    holder = (ViewHolderLeftText) convertView.getTag();
                }
                disPlayLeftTextView(position, convertView, holder, bean);
                break;
            case VIEW_TYPE_LEFT_IMAGE:
                ViewHolderLeftImage imageHolder;
                if (convertView == null) {
                    imageHolder = new ViewHolderLeftImage();
                    holderView = mLayoutScatter.parse(ResourceTable.Layout_listitem_chat_left_image, null, false);
                    holderView.setFocusable(Component.FOCUS_ENABLE);
                    imageHolder.iv_avatar = (Image) holderView.findComponentById(ResourceTable.Id_iv_avatar);
                    imageHolder.iv_image = (Image) holderView.findComponentById(ResourceTable.Id_iv_image);
                    holderView.setTag(imageHolder);
                    convertView = holderView;
                } else {
                    imageHolder = (ViewHolderLeftImage) convertView.getTag();
                }
                disPlayLeftImageView(position, convertView, imageHolder, bean);
                break;
            default:
                convertView = new Component(mActivity);
                break;
        }
        return convertView;
    }

    public void disPlayLeftTextView(int position, Component view, ViewHolderLeftText holder, ImMsgBean bean) {
        setContent(holder.tv_content, bean.getContent());
    }

    public void setContent(Text tv_content, String content) {
        SimpleCommonUtils.spannableEmoticonFilter(tv_content, content);
    }

    public void disPlayLeftImageView(int position, Component view, ViewHolderLeftImage holder, ImMsgBean bean) {
        try {
            ImageLoader.getInstance(mActivity).displayImage(bean.getImage(), holder.iv_image);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public final class ViewHolderLeftText {
        public Image iv_avatar;
        public Text tv_content;
    }

    public final class ViewHolderLeftImage {
        public Image iv_avatar;
        public Image iv_image;
    }
}