package com.example.xhsemoticonskeyboard.common.adapter;

import com.example.xhsemoticonskeyboard.ResourceTable;
import com.example.xhsemoticonskeyboard.common.data.AppBean;
import ohos.agp.components.*;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.ToastDialog;
import ohos.app.Context;
import sj.keyboard.utils.EmoticonsKeyboardUtils;

import java.util.ArrayList;

public class AppsAdapter extends BaseItemProvider {

    private LayoutScatter mLayoutScatter;
    private Context mContext;
    private ArrayList<AppBean> mData = new ArrayList<AppBean>();

    public AppsAdapter(Context context, ArrayList<AppBean> data) {
        this.mContext = context;
        this.mLayoutScatter = LayoutScatter.getInstance(context);
        if (data != null) {
            this.mData = data;
        }
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public Object getItem(int position) {
        return mData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Component getComponent(int position, Component convertView, ComponentContainer parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = mLayoutScatter.parse(ResourceTable.Layout_item_app, null, false);

            ComponentContainer.LayoutConfig layoutConfig = convertView.getLayoutConfig();
            int viewSize = EmoticonsKeyboardUtils.getDisplayWidthPixels(mContext) / 4;
            layoutConfig.width = viewSize;
            layoutConfig.height = viewSize + EmoticonsKeyboardUtils.vp2px(mContext, 20);
            convertView.setLayoutConfig(layoutConfig);

            viewHolder.iv_icon = (Image) convertView.findComponentById(ResourceTable.Id_iv_icon);
            viewHolder.tv_name = (Text) convertView.findComponentById(ResourceTable.Id_tv_name);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        final AppBean appBean = mData.get(position);
        if (appBean != null) {
            viewHolder.iv_icon.setPixelMap(appBean.getIcon());
            viewHolder.tv_name.setText(appBean.getFuncName());
            convertView.setClickedListener(new Component.ClickedListener() {
                @Override
                public void onClick(Component v) {
                    new ToastDialog(mContext)
                            .setText(appBean.getFuncName())
                            .setAlignment(LayoutAlignment.CENTER)
                            .setDuration(3000)
                            .show();
                }
            });
        }
        return convertView;
    }

    class ViewHolder {
        public Image iv_icon;
        public Text tv_name;
    }
}