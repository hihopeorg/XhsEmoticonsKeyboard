package com.example.xhsemoticonskeyboard.slice;

import com.example.xhsemoticonskeyboard.ResourceTable;
import com.example.xhsemoticonskeyboard.common.Constants;
import com.example.xhsemoticonskeyboard.common.SimpleCommonUtils;
import com.example.xhsemoticonskeyboard.common.adapter.ChattingListAdapter;
import com.example.xhsemoticonskeyboard.common.data.ImMsgBean;
import com.example.xhsemoticonskeyboard.userdef.SimpleUserDefAppsGridView;
import com.example.xhsemoticonskeyboard.userdef.SimpleUserdefEmoticonsKeyBoard;
import com.sj.emoji.EmojiBean;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.ListContainer;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.ToastDialog;
import ohos.agp.window.service.WindowManager;
import ohos.multimodalinput.event.TouchEvent;
import sj.keyboard.data.EmoticonEntity;
import sj.keyboard.interfaces.EmoticonClickListener;

import java.util.ArrayList;
import java.util.List;

public class SimpleChatUserDefAbilitySlice extends AbilitySlice {

    private ListContainer lvChat;
    private SimpleUserdefEmoticonsKeyBoard ekBar;

    private ChattingListAdapter chattingListAdapter;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_simple_chat_user_def);

        getWindow().setInputPanelDisplayType(WindowManager.LayoutConfig.INPUT_ADJUST_PAN);

        lvChat = (ListContainer) findComponentById(ResourceTable.Id_lv_chat);
        ekBar = (SimpleUserdefEmoticonsKeyBoard) findComponentById(ResourceTable.Id_ek_bars);

        initEmoticonsKeyBoardBar();
        initListView();
    }

    @Override
    protected void onInactive() {
        super.onInactive();
        ekBar.reset();
    }

    private void initEmoticonsKeyBoardBar() {
        ekBar.setKeyboardVisibilityEvent(this, () -> scrollToBottom());

        SimpleCommonUtils.initEmoticonsEditText(ekBar.getEtChat());
        ekBar.setAdapter(SimpleCommonUtils.getCommonAdapter(this, emoticonClickListener));
        ekBar.addFuncView(new SimpleUserDefAppsGridView(this));

        ekBar.getEtChat().setOnSizeChangedListener((w, h, oldw, oldh) -> scrollToBottom());
        ekBar.getBtnSend().setClickedListener(v -> {
            OnSendBtnClick(ekBar.getEtChat().getText());
            ekBar.getEtChat().setText("");
        });
        ekBar.getEmoticonsToolBarView().addFixedToolItemView(false, ResourceTable.Media_icon_add, null, v ->
                new ToastDialog(SimpleChatUserDefAbilitySlice.this)
                        .setText("ADD")
                        .setAlignment(LayoutAlignment.CENTER)
                        .setDuration(3000)
                        .show());
        ekBar.getEmoticonsToolBarView().addToolItemView(ResourceTable.Media_icon_setting, v ->
                new ToastDialog(SimpleChatUserDefAbilitySlice.this)
                        .setText("SETTING")
                        .setAlignment(LayoutAlignment.CENTER)
                        .setDuration(3000)
                        .show());
    }

    private EmoticonClickListener emoticonClickListener = new EmoticonClickListener() {
        @Override
        public void onEmoticonClick(Object o, int actionType, boolean isDelBtn) {

            if (isDelBtn) {
                SimpleCommonUtils.delClick(ekBar.getEtChat());
            } else {
                if (o == null) {
                    return;
                }
                if (actionType == Constants.EMOTICON_CLICK_BIGIMAGE) {
                    if (o instanceof EmoticonEntity) {
                        OnSendImage(((EmoticonEntity) o).getIconUri());
                    }
                } else {
                    String content = null;
                    if (o instanceof EmojiBean) {
                        content = ((EmojiBean) o).emoji;
                    } else if (o instanceof EmoticonEntity) {
                        content = ((EmoticonEntity) o).getContent();
                    }

                    if (content == null || content.isEmpty()) {
                        return;
                    }

                    ekBar.getEtChat().insert(content);
                }
            }
        }
    };

    private void initListView() {
        chattingListAdapter = new ChattingListAdapter(this);
        List<ImMsgBean> beanList = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            ImMsgBean bean = new ImMsgBean();
            bean.setContent("Test:" + i);
            beanList.add(bean);
        }
        chattingListAdapter.addData(beanList);
        lvChat.setItemProvider(chattingListAdapter);
        scrollToBottom();
        lvChat.setTouchEventListener((component, touchEvent) -> {
            if (touchEvent.getAction() == TouchEvent.PRIMARY_POINT_DOWN) {
                ekBar.reset();
            }
            return false;
        });
    }

    private void OnSendBtnClick(String msg) {
        if (msg != null && !msg.isEmpty()) {
            ImMsgBean bean = new ImMsgBean();
            bean.setContent(msg);
            chattingListAdapter.addData(bean, true, false);
            scrollToBottom();
        }
    }

    private void OnSendImage(String image) {
        if (image != null && !image.isEmpty()) {
            OnSendBtnClick("[img]" + image);
        }
    }

    private void scrollToBottom() {
        lvChat.scrollTo(chattingListAdapter.getCount() - 1);
    }
}
