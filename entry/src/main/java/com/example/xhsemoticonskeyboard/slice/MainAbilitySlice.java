package com.example.xhsemoticonskeyboard.slice;

import com.example.xhsemoticonskeyboard.*;
import com.example.xhsemoticonskeyboard.SimpleChatUserDefAbility;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.window.dialog.CommonDialog;
import ohos.bundle.IBundleManager;
import ohos.utils.net.Uri;
import sj.keyboard.utils.EmoticonsKeyboardUtils;

public class MainAbilitySlice extends AbilitySlice {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        findComponentById(ResourceTable.Id_btn_simple_comment).setClickedListener(component ->
                startAbility(SimpleCommentAbility.class.getSimpleName()));
        findComponentById(ResourceTable.Id_btn_simple_chat_fullscreen).setClickedListener(component ->
                startAbility(SimpleChatAbility.class.getSimpleName()));
        findComponentById(ResourceTable.Id_btn_simple_chat_translucent).setClickedListener(component ->
                startAbility(SimpleTranslucentChatAbility.class.getSimpleName()));
        findComponentById(ResourceTable.Id_btn_simple_chat_coordinatorlayout).setClickedListener(component ->
                startAbility(SimpleChatOnCoordinatorLayoutAbility.class.getSimpleName()));
        findComponentById(ResourceTable.Id_btn_userdef_ui).setClickedListener(component ->
                startAbility(SimpleChatUserDefAbility.class.getSimpleName()));
        findComponentById(ResourceTable.Id_btn_qq).setClickedListener(component ->
                startAbility(QqAbility.class.getSimpleName()));
        findComponentById(ResourceTable.Id_btn_github).setClickedListener(component -> {
            Intent i = new Intent();
            i.setUri(Uri.parse("http://github.com/w446108264/XhsEmoticonsKeyboard"));
            startAbility(i);
        });

        requestPermission();
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    private void startAbility(String abilityNam) {
        Intent intent = new Intent();
        Operation operation = new Intent.OperationBuilder()
                .withBundleName(getBundleName())
                .withAbilityName(abilityNam)
                .build();
        intent.setOperation(operation);
        startAbility(intent);
    }

    private void requestPermission() {
        if (verifySelfPermission("ohos.permission.READ_MEDIA") != IBundleManager.PERMISSION_GRANTED) {
            if (canRequestPermission("ohos.permission.READ_MEDIA")) {
                new CommonDialog(this)
                        .setContentText("为了正常解压和加载表情,需要您的授权.")
                        .setButton(1, "确定", (iDialog, i) -> {
                            iDialog.destroy();
                            requestPermissionsFromUser(
                                    new String[]{"ohos.permission.READ_MEDIA"}, 0);
                        })
                        .setSize(EmoticonsKeyboardUtils.getDisplayWidthPixels(this) - 100, 200)
                        .show();
            }
        }
    }
}
