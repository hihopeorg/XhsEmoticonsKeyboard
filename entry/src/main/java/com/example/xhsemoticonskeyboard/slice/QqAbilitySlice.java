package com.example.xhsemoticonskeyboard.slice;

import com.example.xhsemoticonskeyboard.ResourceTable;
import com.example.xhsemoticonskeyboard.common.Constants;
import com.example.xhsemoticonskeyboard.common.data.ImMsgBean;
import com.example.xhsemoticonskeyboard.qq.QqChattingListAdapter;
import com.example.xhsemoticonskeyboard.qq.QqEmoticonsKeyBoard;
import com.example.xhsemoticonskeyboard.qq.QqUtils;
import com.example.xhsemoticonskeyboard.qq.SimpleQqGridView;
import com.sj.emoji.EmojiBean;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.ListContainer;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.ToastDialog;
import ohos.agp.window.service.WindowManager;
import ohos.multimodalinput.event.TouchEvent;
import sj.keyboard.data.EmoticonEntity;
import sj.keyboard.interfaces.EmoticonClickListener;

import java.util.ArrayList;
import java.util.List;

public class QqAbilitySlice extends AbilitySlice {

    private ListContainer lvChat;
    private QqEmoticonsKeyBoard ekBar;

    private QqChattingListAdapter chattingListAdapter;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_qq);

        getWindow().setInputPanelDisplayType(WindowManager.LayoutConfig.INPUT_ADJUST_PAN);

        lvChat = (ListContainer) findComponentById(ResourceTable.Id_lv_chat);
        ekBar = (QqEmoticonsKeyBoard) findComponentById(ResourceTable.Id_ek_bar);

        initView();
    }


    @Override
    protected void onInactive() {
        super.onInactive();
        ekBar.reset();
    }

    private void initView() {
        initEmoticonsKeyBoardBar();
        initListView();
    }

    private void initEmoticonsKeyBoardBar() {
        ekBar.setKeyboardVisibilityEvent(this, () -> scrollToBottom());

        QqUtils.initEmoticonsEditText(ekBar.getEtChat());
        ekBar.setAdapter(QqUtils.getCommonAdapter(this, emoticonClickListener));

        ekBar.addFuncView(QqEmoticonsKeyBoard.FUNC_TYPE_PTT, new SimpleQqGridView(this));
        ekBar.addFuncView(QqEmoticonsKeyBoard.FUNC_TYPE_PTV, new SimpleQqGridView(this));
        ekBar.addFuncView(QqEmoticonsKeyBoard.FUNC_TYPE_PLUG, new SimpleQqGridView(this));

        ekBar.getEtChat().setOnSizeChangedListener((w, h, oldw, oldh) -> scrollToBottom());
        ekBar.getBtnSend().setClickedListener(v -> {
            OnSendBtnClick(ekBar.getEtChat().getText());
            ekBar.getEtChat().setText("");
        });
        ekBar.getEmoticonsToolBarView().addFixedToolItemView(true, ResourceTable.Media_qvip_emoji_tab_more_new_pressed, null, v ->
                new ToastDialog(QqAbilitySlice.this)
                        .setText("ADD")
                        .setAlignment(LayoutAlignment.CENTER)
                        .setDuration(3000)
                        .show());
    }

    private EmoticonClickListener emoticonClickListener = new EmoticonClickListener() {
        @Override
        public void onEmoticonClick(Object o, int actionType, boolean isDelBtn) {

            if (isDelBtn) {
                QqUtils.delClick(ekBar.getEtChat());
            } else {
                if (o == null) {
                    return;
                }
                if (actionType == Constants.EMOTICON_CLICK_BIGIMAGE) {
                    if (o instanceof EmoticonEntity) {
                        OnSendImage(((EmoticonEntity) o).getIconUri());
                    }
                } else {
                    String content = null;
                    if (o instanceof EmojiBean) {
                        content = ((EmojiBean) o).emoji;
                    } else if (o instanceof EmoticonEntity) {
                        content = ((EmoticonEntity) o).getContent();
                    }

                    if (content == null || content.isEmpty()) {
                        return;
                    }

                    ekBar.getEtChat().insert(content);
                }
            }
        }
    };

    private void initListView() {
        chattingListAdapter = new QqChattingListAdapter(this);
        List<ImMsgBean> beanList = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            ImMsgBean bean = new ImMsgBean();
            bean.setContent("Test:" + i);
            beanList.add(bean);
        }
        chattingListAdapter.addData(beanList);
        lvChat.setItemProvider(chattingListAdapter);
        scrollToBottom();
        lvChat.setTouchEventListener((component, touchEvent) -> {
            if (touchEvent.getAction() == TouchEvent.PRIMARY_POINT_DOWN) {
                ekBar.reset();
            }
            return false;
        });
    }

    private void OnSendBtnClick(String msg) {
        if (msg != null && !msg.isEmpty()) {
            ImMsgBean bean = new ImMsgBean();
            bean.setContent(msg);
            chattingListAdapter.addData(bean, true, false);
            scrollToBottom();
        }
    }

    private void OnSendImage(String image) {
        if (image != null && !image.isEmpty()) {
            OnSendBtnClick("[img]" + image);
        }
    }

    private void scrollToBottom() {
        lvChat.scrollTo(chattingListAdapter.getCount() - 1);
    }
}
