package com.example.xhsemoticonskeyboard.slice;

import com.example.xhsemoticonskeyboard.ResourceTable;
import com.example.xhsemoticonskeyboard.common.SimpleCommonUtils;
import com.example.xhsemoticonskeyboard.common.adapter.ChattingListAdapter;
import com.example.xhsemoticonskeyboard.common.data.ImMsgBean;
import com.example.xhsemoticonskeyboard.common.widget.SimpleAppsGridView;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.ListContainer;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.ToastDialog;
import ohos.agp.window.service.WindowManager;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.multimodalinput.event.TouchEvent;
import sj.keyboard.XhsEmoticonsKeyBoard;
import sj.keyboard.widget.AutoHeightLayout;

import java.util.ArrayList;
import java.util.List;

public class SimpleChatOnCoordinatorLayoutAbilitySlice extends AbilitySlice implements AutoHeightLayout.OnMaxParentHeightChangeListener {

    private ListContainer lvChat;
    private XhsEmoticonsKeyBoard ekBar;

    private ChattingListAdapter chattingListAdapter;

    private final EventHandler mHandler = new EventHandler(EventRunner.getMainEventRunner());

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_simple_chat_on_coordinator_layout);

        getWindow().setInputPanelDisplayType(WindowManager.LayoutConfig.INPUT_ADJUST_PAN);

        lvChat = (ListContainer) findComponentById(ResourceTable.Id_lv_chat);
        ekBar = (XhsEmoticonsKeyBoard) findComponentById(ResourceTable.Id_ek_bar);

        initEmoticonsKeyBoardBar();
        initListView();
    }

    @Override
    protected void onInactive() {
        super.onInactive();
        ekBar.reset();
    }

    private void initEmoticonsKeyBoardBar() {
        ekBar.setKeyboardVisibilityEvent(this, () -> scrollToBottom());

        SimpleCommonUtils.initEmoticonsEditText(ekBar.getEtChat());
        ekBar.setAdapter(SimpleCommonUtils.getCommonAdapter(this, null));
        ekBar.addFuncView(new SimpleAppsGridView(this));
        ekBar.setOnMaxParentHeightChangeListener(this);

        ekBar.getBtnSend().setClickedListener(v -> {
            OnSendBtnClick(ekBar.getEtChat().getText());
            ekBar.getEtChat().setText("");
        });

        ekBar.getEmoticonsToolBarView().addToolItemView(com.keyboard.view.ResourceTable.Media_icon_face_nomal, v ->
                new ToastDialog(SimpleChatOnCoordinatorLayoutAbilitySlice.this)
                        .setText("TEST")
                        .setAlignment(LayoutAlignment.CENTER)
                        .setDuration(3000)
                        .show());
        ekBar.getEmoticonsToolBarView().addToolItemView(com.keyboard.view.ResourceTable.Media_icon_face_nomal, null);
        ekBar.getEmoticonsToolBarView().addToolItemView(com.keyboard.view.ResourceTable.Media_icon_face_nomal, null);
        ekBar.getEmoticonsToolBarView().addToolItemView(com.keyboard.view.ResourceTable.Media_icon_face_nomal, null);
        ekBar.getEmoticonsToolBarView().addToolItemView(com.keyboard.view.ResourceTable.Media_icon_face_nomal, null);
        ekBar.getEmoticonsToolBarView().addToolItemView(com.keyboard.view.ResourceTable.Media_icon_face_nomal, null);
        ekBar.getEmoticonsToolBarView().addToolItemView(com.keyboard.view.ResourceTable.Media_icon_face_nomal, null);
        ekBar.getEmoticonsToolBarView().addToolItemView(com.keyboard.view.ResourceTable.Media_icon_face_nomal, null);
    }

    private void initListView() {
        chattingListAdapter = new ChattingListAdapter(this);
        List<ImMsgBean> beanList = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            ImMsgBean bean = new ImMsgBean();
            bean.setContent("Test:" + i);
            beanList.add(bean);
        }
        chattingListAdapter.addData(beanList);
        lvChat.setItemProvider(chattingListAdapter);
        scrollToBottom();
        lvChat.setTouchEventListener((component, touchEvent) -> {
            if (touchEvent.getAction() == TouchEvent.PRIMARY_POINT_DOWN) {
                ekBar.reset();
            }
            return false;
        });
    }

    private void OnSendBtnClick(String msg) {
        if (msg != null && !msg.isEmpty()) {
            ImMsgBean bean = new ImMsgBean();
            bean.setContent(msg);
            chattingListAdapter.addData(bean, true, false);
            scrollToBottom();
        }
    }

    private void scrollToBottom() {
        lvChat.scrollTo(chattingListAdapter.getCount() - 1);
    }

    @Override
    public void onMaxParentHeightChange(int height) {
        scrollToBottom();
    }
}
