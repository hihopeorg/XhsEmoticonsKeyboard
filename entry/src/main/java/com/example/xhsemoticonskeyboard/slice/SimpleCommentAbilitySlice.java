package com.example.xhsemoticonskeyboard.slice;

import com.example.xhsemoticonskeyboard.ResourceTable;
import com.example.xhsemoticonskeyboard.common.SimpleCommonUtils;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.ToastDialog;
import ohos.multimodalinput.event.KeyEvent;
import sj.keyboard.EmoticonsKeyBoardPopWindow;
import sj.keyboard.adpater.PageSetAdapter;
import sj.keyboard.interfaces.EmoticonClickListener;
import sj.keyboard.widget.EmoticonsEditText;

public class SimpleCommentAbilitySlice extends AbilitySlice {

    private EmoticonsEditText etContent;

    private EmoticonsKeyBoardPopWindow mKeyBoardPopWindow = null;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_simple_comment);
        etContent = (EmoticonsEditText) findComponentById(ResourceTable.Id_et_content);
        findComponentById(ResourceTable.Id_tv_at).setClickedListener(component -> {
            new ToastDialog(SimpleCommentAbilitySlice.this)
                    .setText("@")
                    .setAlignment(LayoutAlignment.CENTER)
                    .setDuration(3000)
                    .show();
        });
        findComponentById(ResourceTable.Id_tv_topic).setClickedListener(component -> {
            new ToastDialog(SimpleCommentAbilitySlice.this)
                    .setText("#")
                    .setAlignment(LayoutAlignment.CENTER)
                    .setDuration(3000)
                    .show();
        });
        findComponentById(ResourceTable.Id_iv_face).setClickedListener(component -> {
            if (mKeyBoardPopWindow != null && mKeyBoardPopWindow.isShowing()) {
                mKeyBoardPopWindow.destroy();
                mKeyBoardPopWindow = null;
            } else {
                if (mKeyBoardPopWindow == null) {
                    initKeyBoardPopWindow();
                }
                mKeyBoardPopWindow.showPopupWindow();
            }
        });

        initEmoticonsEditText();
    }


    private void initEmoticonsEditText() {
        SimpleCommonUtils.initEmoticonsEditText(etContent);
        etContent.setFocusable(Component.FOCUS_ENABLE);
        etContent.setTouchFocusable(true);
        etContent.requestFocus();
    }

    private void initKeyBoardPopWindow() {
        mKeyBoardPopWindow = new EmoticonsKeyBoardPopWindow(SimpleCommentAbilitySlice.this);

        EmoticonClickListener emoticonClickListener = SimpleCommonUtils.getCommonEmoticonClickListener(etContent);
        PageSetAdapter pageSetAdapter = new PageSetAdapter();
        SimpleCommonUtils.addEmojiPageSetEntity(pageSetAdapter, this, emoticonClickListener);
        SimpleCommonUtils.addXhsPageSetEntity(pageSetAdapter, this, emoticonClickListener);
        mKeyBoardPopWindow.setAdapter(pageSetAdapter);

        mKeyBoardPopWindow.setDialogListener(() -> {
            mKeyBoardPopWindow.destroy();
            mKeyBoardPopWindow = null;
            return false;
        });
    }


    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent keyEvent) {
        if (keyEvent.getKeyCode() == KeyEvent.KEY_BACK
                && mKeyBoardPopWindow != null && mKeyBoardPopWindow.isShowing()) {
            mKeyBoardPopWindow.destroy();
            return false;
        }
        return super.onKeyDown(keyCode, keyEvent);
    }
}
