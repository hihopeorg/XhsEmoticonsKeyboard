package com.gelitenight.waveview.sample.automation;

import com.example.xhsemoticonskeyboard.*;
import ohos.aafwk.ability.Ability;
import ohos.agp.components.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import sj.keyboard.utils.EmoticonsKeyboardUtils;
import sj.keyboard.utils.IdUtils;
import sj.keyboard.widget.EmoticonsEditText;

import static org.junit.Assert.assertNotNull;

public class UiTest {

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
        EventHelper.clearAbilities();
        delayTwoSeconds();
    }

    @Test
    public void simpleCommentKeyboardTest() {
        Ability simpleCommentAbility = EventHelper.startAbility(SimpleCommentAbility.class);
        Component atComponent = simpleCommentAbility.findComponentById(ResourceTable.Id_tv_at);
        assertNotNull("It's right", atComponent);
        EventHelper.triggerClickEvent(simpleCommentAbility, atComponent);
        delayTwoSeconds();
        Component topicComponent = simpleCommentAbility.findComponentById(ResourceTable.Id_tv_topic);
        assertNotNull("It's right", topicComponent);
        EventHelper.triggerClickEvent(simpleCommentAbility, topicComponent);
        delayTwoSeconds();
        TextField etContent = (TextField) simpleCommentAbility.findComponentById(ResourceTable.Id_et_content);
        assertNotNull("It's right", etContent);
        etContent.setText("text");
        delayTwoSeconds();
        Component faceComponent = simpleCommentAbility.findComponentById(ResourceTable.Id_iv_face);
        assertNotNull("It's right", faceComponent);
        EventHelper.triggerClickEvent(simpleCommentAbility, faceComponent);
        delayTwoSeconds();
    }

    @Test
    public void simpleChatVoiceTest() {
        Ability simpleChatAbility = EventHelper.startAbility(SimpleChatAbility.class);
        Component btnVoiceOrText = simpleChatAbility
                .findComponentById(com.keyboard.view.ResourceTable.Id_btn_voice_or_text);
        assertNotNull("It's right", btnVoiceOrText);
        EventHelper.triggerClickEvent(simpleChatAbility, btnVoiceOrText);
        delayTwoSeconds();
        EventHelper.triggerClickEvent(simpleChatAbility, btnVoiceOrText);
        delayTwoSeconds();
    }

    @Test
    public void simpleChatMultimediaTest() {
        Ability simpleChatAbility = EventHelper.startAbility(SimpleChatAbility.class);
        Component btnMultimedia = simpleChatAbility
                .findComponentById(com.keyboard.view.ResourceTable.Id_btn_multimedia);
        assertNotNull("It's right", btnMultimedia);
        EventHelper.triggerClickEvent(simpleChatAbility, btnMultimedia);
        delayTwoSeconds();
        ListContainer gv_apps = (ListContainer) simpleChatAbility.findComponentById(ResourceTable.Id_gv_apps);
        assertNotNull("It's right", gv_apps);
        EventHelper.triggerClickEvent(simpleChatAbility, gv_apps.getComponentAt(0));
        delayTwoSeconds();
        EventHelper.triggerClickEvent(simpleChatAbility, btnMultimedia);
        delayTwoSeconds();
    }

    @Test
    public void simpleChatFaceTest() {
        Ability simpleChatAbility = EventHelper.startAbility(SimpleChatAbility.class);
        Component btnFace = simpleChatAbility.findComponentById(com.keyboard.view.ResourceTable.Id_btn_face);
        assertNotNull("It's right", btnFace);
        EventHelper.triggerClickEvent(simpleChatAbility, btnFace);
        delayTwoSeconds();
        PageSlider emoticonsFuncView = (PageSlider) simpleChatAbility
                .findComponentById(com.keyboard.view.ResourceTable.Id_view_epv);
        assertNotNull("It's right", emoticonsFuncView);
        ListContainer gvEmotion = (ListContainer) emoticonsFuncView.getComponentAt(0)
                .findComponentById(com.keyboard.view.ResourceTable.Id_gv_emotion);
        assertNotNull("It's right", gvEmotion);
        EventHelper.triggerClickEvent(simpleChatAbility, gvEmotion.getComponentAt(0));
        delayTwoSeconds();
        Button btnSend = ((Button) simpleChatAbility.findComponentById(com.keyboard.view.ResourceTable.Id_btn_send));
        assertNotNull("It's right", btnSend);
        EventHelper.triggerClickEvent(simpleChatAbility, btnSend);
        delayTwoSeconds();
    }

    @Test
    public void chatListSlideTest() {
        Ability simpleTranslucentChatAbility = EventHelper.startAbility(SimpleTranslucentChatAbility.class);
        ListContainer lvChat = (ListContainer) simpleTranslucentChatAbility
                .findComponentById(ResourceTable.Id_lv_chat);
        assertNotNull("It's right", lvChat);
        EventHelper.inputSwipe(simpleTranslucentChatAbility,
                lvChat.getWidth() / 2, AttrHelper.vp2px(500, simpleTranslucentChatAbility.getContext()),
                lvChat.getWidth() / 2, AttrHelper.vp2px(100, simpleTranslucentChatAbility.getContext()),
                1000);
        delayTwoSeconds();
    }

    @Test
    public void fixedToolItemAndSettingTest() {
        Ability simpleTranslucentChatAbility = EventHelper.startAbility(SimpleTranslucentChatAbility.class);
        Component btnFace = simpleTranslucentChatAbility.findComponentById(com.keyboard.view.ResourceTable.Id_btn_face);
        assertNotNull("It's right", btnFace);
        EventHelper.triggerClickEvent(simpleTranslucentChatAbility, btnFace);
        delayTwoSeconds();
        Component toolBarLeft = simpleTranslucentChatAbility.findComponentById(IdUtils.ID_TOOLBAR_LEFT);
        assertNotNull("It's right", toolBarLeft);
        EventHelper.triggerClickEvent(simpleTranslucentChatAbility, toolBarLeft);
        delayTwoSeconds();
        DirectionalLayout lyTool = (DirectionalLayout) simpleTranslucentChatAbility
                .findComponentById(ResourceTable.Id_ly_tool);
        assertNotNull("It's right", lyTool);
        EventHelper.triggerClickEvent(simpleTranslucentChatAbility,
                lyTool.getComponentAt(lyTool.getChildCount() - 1));
        delayTwoSeconds();
    }

    @Test
    public void emojiPageTest() {
        Ability simpleTranslucentChatAbility = EventHelper.startAbility(SimpleTranslucentChatAbility.class);
        Component btnFace = simpleTranslucentChatAbility.findComponentById(com.keyboard.view.ResourceTable.Id_btn_face);
        assertNotNull("It's right", btnFace);
        EventHelper.triggerClickEvent(simpleTranslucentChatAbility, btnFace);
        delayTwoSeconds();
        DirectionalLayout lyTool = (DirectionalLayout) simpleTranslucentChatAbility
                .findComponentById(ResourceTable.Id_ly_tool);
        assertNotNull("It's right", lyTool);
        EventHelper.triggerClickEvent(simpleTranslucentChatAbility, lyTool.getComponentAt(0));
        delayTwoSeconds();
    }

    @Test
    public void xhsPageTest() {
        Ability simpleTranslucentChatAbility = EventHelper.startAbility(SimpleTranslucentChatAbility.class);
        Component btnFace = simpleTranslucentChatAbility.findComponentById(com.keyboard.view.ResourceTable.Id_btn_face);
        assertNotNull("It's right", btnFace);
        EventHelper.triggerClickEvent(simpleTranslucentChatAbility, btnFace);
        delayTwoSeconds();
        DirectionalLayout lyTool = (DirectionalLayout) simpleTranslucentChatAbility
                .findComponentById(ResourceTable.Id_ly_tool);
        assertNotNull("It's right", lyTool);
        EventHelper.triggerClickEvent(simpleTranslucentChatAbility, lyTool.getComponentAt(1));
        delayTwoSeconds();
    }

    @Test
    public void wechatPageTest() {
        Ability simpleTranslucentChatAbility = EventHelper.startAbility(SimpleTranslucentChatAbility.class);
        Component btnFace = simpleTranslucentChatAbility.findComponentById(com.keyboard.view.ResourceTable.Id_btn_face);
        assertNotNull("It's right", btnFace);
        EventHelper.triggerClickEvent(simpleTranslucentChatAbility, btnFace);
        delayTwoSeconds();
        DirectionalLayout lyTool = (DirectionalLayout) simpleTranslucentChatAbility
                .findComponentById(ResourceTable.Id_ly_tool);
        assertNotNull("It's right", lyTool);
        EventHelper.triggerClickEvent(simpleTranslucentChatAbility, lyTool.getComponentAt(2));
        delayTwoSeconds();
    }

    @Test
    public void goodGoodStudyPageTest() {
        Ability simpleTranslucentChatAbility = EventHelper.startAbility(SimpleTranslucentChatAbility.class);
        Component btnFace = simpleTranslucentChatAbility.findComponentById(com.keyboard.view.ResourceTable.Id_btn_face);
        assertNotNull("It's right", btnFace);
        EventHelper.triggerClickEvent(simpleTranslucentChatAbility, btnFace);
        delayTwoSeconds();
        DirectionalLayout lyTool = (DirectionalLayout) simpleTranslucentChatAbility
                .findComponentById(ResourceTable.Id_ly_tool);
        assertNotNull("It's right", lyTool);
        EventHelper.triggerClickEvent(simpleTranslucentChatAbility, lyTool.getComponentAt(3));
        delayTwoSeconds();
    }

    @Test
    public void pageSlideTest() {
        Ability simpleChatOnCoordinatorLayoutAbility = EventHelper
                .startAbility(SimpleChatOnCoordinatorLayoutAbility.class);
        Component btnFace = simpleChatOnCoordinatorLayoutAbility
                .findComponentById(com.keyboard.view.ResourceTable.Id_btn_face);
        assertNotNull("It's right", btnFace);
        EventHelper.triggerClickEvent(simpleChatOnCoordinatorLayoutAbility, btnFace);
        delayTwoSeconds();
        int touchStartWidth = EmoticonsKeyboardUtils
                .getDisplayWidthPixels(simpleChatOnCoordinatorLayoutAbility.getContext()) * 3 / 4;
        int touchStopWidth = EmoticonsKeyboardUtils
                .getDisplayWidthPixels(simpleChatOnCoordinatorLayoutAbility.getContext()) * 1 / 4;
        int touchHeight = EmoticonsKeyboardUtils
                .getDisplayHeightPixels(simpleChatOnCoordinatorLayoutAbility.getContext()) * 3 / 4;
        EventHelper.inputSwipe(simpleChatOnCoordinatorLayoutAbility,
                touchStartWidth, touchHeight,
                touchStopWidth, touchHeight,
                1000);
        delayTwoSeconds();
    }

    @Test
    public void otherFaceToolbarTest() {
        Ability simpleChatOnCoordinatorLayoutAbility = EventHelper
                .startAbility(SimpleChatOnCoordinatorLayoutAbility.class);
        Component btnFace = simpleChatOnCoordinatorLayoutAbility
                .findComponentById(com.keyboard.view.ResourceTable.Id_btn_face);
        assertNotNull("It's right", btnFace);
        EventHelper.triggerClickEvent(simpleChatOnCoordinatorLayoutAbility, btnFace);
        delayTwoSeconds();
        DirectionalLayout lyTool = (DirectionalLayout) simpleChatOnCoordinatorLayoutAbility
                .findComponentById(ResourceTable.Id_ly_tool);
        assertNotNull("It's right", lyTool);
        EventHelper.triggerClickEvent(simpleChatOnCoordinatorLayoutAbility, lyTool.getComponentAt(6));
        delayTwoSeconds();
        EventHelper.triggerClickEvent(simpleChatOnCoordinatorLayoutAbility, lyTool.getComponentAt(7));
        delayTwoSeconds();
    }

    @Test
    public void simpleChatUserDefTest() {
        Ability simpleChatUserDefAbility = EventHelper.startAbility(SimpleChatUserDefAbility.class);

        Component btnVoiceOrText = simpleChatUserDefAbility
                .findComponentById(com.keyboard.view.ResourceTable.Id_btn_voice_or_text);
        assertNotNull("It's right", btnVoiceOrText);
        EventHelper.triggerClickEvent(simpleChatUserDefAbility, btnVoiceOrText);
        delayTwoSeconds();
        EventHelper.triggerClickEvent(simpleChatUserDefAbility, btnVoiceOrText);
        delayTwoSeconds();
        Component btnMultimedia = simpleChatUserDefAbility
                .findComponentById(com.keyboard.view.ResourceTable.Id_btn_multimedia);
        assertNotNull("It's right", btnMultimedia);
        EventHelper.triggerClickEvent(simpleChatUserDefAbility, btnMultimedia);
        delayTwoSeconds();
        ListContainer gvApps = (ListContainer) simpleChatUserDefAbility.findComponentById(ResourceTable.Id_gv_apps);
        EventHelper.triggerClickEvent(simpleChatUserDefAbility, gvApps.getComponentAt(0));
        delayTwoSeconds();
    }

    @Test
    public void qqKeyBoardTest() {
        Ability qqAbility = EventHelper.startAbility(QqAbility.class);

        TextField etChat = (EmoticonsEditText) qqAbility.findComponentById(ResourceTable.Id_et_chat);
        assertNotNull("It's right", etChat);
        etChat.setText("text");
        Button btnSend = ((Button) qqAbility.findComponentById(com.keyboard.view.ResourceTable.Id_btn_send));
        assertNotNull("It's right", btnSend);
        EventHelper.triggerClickEvent(qqAbility, btnSend);
        Component btnVoice = qqAbility.findComponentById(com.keyboard.view.ResourceTable.Id_btn_voice);
        assertNotNull("It's right", btnVoice);
        EventHelper.triggerClickEvent(qqAbility, btnVoice);
        delayTwoSeconds();
        EventHelper.triggerClickEvent(qqAbility, btnVoice);
        delayTwoSeconds();
        Component btnPlug = qqAbility.findComponentById(ResourceTable.Id_btn_plug);
        assertNotNull("It's right", btnPlug);
        EventHelper.triggerClickEvent(qqAbility, btnPlug);
        delayTwoSeconds();
        ListContainer qqApps = (ListContainer) qqAbility.findComponentById(ResourceTable.Id_gv_apps);
        assertNotNull("It's right", qqApps);
        EventHelper.triggerClickEvent(qqAbility, qqApps.getComponentAt(0));
        delayTwoSeconds();
    }

    private void delayTwoSeconds() {
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}