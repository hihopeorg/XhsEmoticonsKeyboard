package com.gelitenight.waveview.sample.unit;

import com.example.xhsemoticonskeyboard.common.widget.SimpleAppsGridView;
import com.keyboard.view.ResourceTable;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.agp.components.*;
import ohos.app.Context;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import sj.keyboard.XhsEmoticonsKeyBoard;
import sj.keyboard.data.PageEntity;
import sj.keyboard.data.PageSetEntity;
import sj.keyboard.utils.EmoticonsKeyboardUtils;
import sj.keyboard.utils.imageloader.ImageLoader;
import sj.keyboard.widget.*;

import java.util.Optional;

import static org.junit.Assert.*;

public class UnitTest {

    private Context context;
    private AttrSet attrSet;

    @Before
    public void setUp() {
        context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        attrSet = new AttrSet() {
            @Override
            public Optional<String> getStyle() {
                return Optional.empty();
            }

            @Override
            public int getLength() {
                return 0;
            }

            @Override
            public Optional<Attr> getAttr(int i) {
                return Optional.empty();
            }

            @Override
            public Optional<Attr> getAttr(String s) {
                return Optional.empty();
            }
        };
    }

    @After
    public void tearDown() {
        context = null;
        attrSet = null;
    }

    @Test
    public void funcLayoutAddFuncView() {
        FuncLayout funcLayout = new FuncLayout(context, attrSet);
        Component keyboardView = LayoutScatter.getInstance(context)
                .parse(ResourceTable.Layout_view_func_emoticon, null, false);
        int childCount = funcLayout.getChildCount();
        funcLayout.addFuncView(XhsEmoticonsKeyBoard.FUNC_TYPE_EMOTION, keyboardView);
        assertTrue("It's right", childCount + 1 == funcLayout.getChildCount());
    }

    @Test
    public void funcLayoutShowFuncView() {
        FuncLayout funcLayout = new FuncLayout(context, attrSet);
        Component keyboardView = LayoutScatter.getInstance(context)
                .parse(ResourceTable.Layout_view_func_emoticon, null, false);
        funcLayout.addFuncView(XhsEmoticonsKeyBoard.FUNC_TYPE_EMOTION, keyboardView);
        SimpleAppsGridView appsGridView = new SimpleAppsGridView(context);
        funcLayout.addFuncView(XhsEmoticonsKeyBoard.FUNC_TYPE_APPPS, appsGridView);
        funcLayout.showFuncView(XhsEmoticonsKeyBoard.FUNC_TYPE_EMOTION);
        assertTrue("It's right", funcLayout.getCurrentFuncKey() == XhsEmoticonsKeyBoard.FUNC_TYPE_EMOTION);
    }

    @Test
    public void funcLayoutHideAllFuncView() {
        FuncLayout funcLayout = new FuncLayout(context, attrSet);
        funcLayout.hideAllFuncView();
        assertTrue("It's right", funcLayout.getCurrentFuncKey() == funcLayout.DEF_KEY);
    }

    @Test
    public void funcLayoutSetVisibility() {
        FuncLayout funcLayout = new FuncLayout(context, attrSet);
        funcLayout.setVisibility(true);
        assertTrue("It's right", funcLayout.getVisibility() == Component.VISIBLE);
        funcLayout.setVisibility(false);
        assertTrue("It's right", funcLayout.getVisibility() == Component.HIDE);
    }

    @Test
    public void funcLayoutAddOnKeyBoardListener() throws InterruptedException {
        FuncLayout funcLayout = new FuncLayout(context, attrSet);
        int KeyboardHeight = EmoticonsKeyboardUtils.getDefKeyboardHeight(context);
        funcLayout.updateHeight(KeyboardHeight);
        FuncLayout.OnFuncKeyBoardListener onFuncKeyBoardListener = new FuncLayout.OnFuncKeyBoardListener() {
            @Override
            public void OnFuncPop(int height) {
                assertTrue("It's right", height == KeyboardHeight);
            }

            @Override
            public void OnFuncClose() {
                assertTrue("It's right", true);
            }
        };
        funcLayout.addOnKeyBoardListener(onFuncKeyBoardListener);
        funcLayout.setVisibility(true);
        Thread.sleep(1000);
        funcLayout.setVisibility(false);
        Thread.sleep(1000);
    }

    @Test
    public void emoticonsToolBarViewAddComponent() {
        EmoticonsToolBarView emoticonsToolBarView = new EmoticonsToolBarView(context, attrSet);
        Component component = LayoutScatter.getInstance(context)
                .parse(ResourceTable.Layout_item_toolbtn, null, false);
        ComponentContainer.LayoutConfig params = new ComponentContainer.LayoutConfig(
                ComponentContainer.LayoutConfig.MATCH_CONTENT, ComponentContainer.LayoutConfig.MATCH_CONTENT);
        int childCount = emoticonsToolBarView.getChildCount();
        emoticonsToolBarView.addComponent(component, 0, params);
        assertTrue("It's right", childCount + 1 == emoticonsToolBarView.getChildCount());
    }

    @Test
    public void emoticonsToolBarViewAddFixedToolItemView() {
        EmoticonsToolBarView emoticonsToolBarView = new EmoticonsToolBarView(context, attrSet);
        Component.ClickedListener clickedListener = component -> {
        };
        int childCount = emoticonsToolBarView.getChildCount();
        emoticonsToolBarView.addFixedToolItemView(false,
                com.example.xhsemoticonskeyboard.ResourceTable.Media_icon_add, null, clickedListener);
        assertTrue("It's right", childCount + 1 == emoticonsToolBarView.getChildCount());
    }

    @Test
    public void emoticonsIndicatorViewPlayTo() {
        EmoticonsIndicatorView emoticonsIndicatorView = new EmoticonsIndicatorView(context, attrSet);
        PageSetEntity pageSetEntity = new PageSetEntity.Builder()
                .addPageEntity(new PageEntity(new SimpleAppsGridView(context)))
                .setShowIndicator(true)
                .build();
        emoticonsIndicatorView.playTo(0, pageSetEntity);
        assertTrue("It's right", emoticonsIndicatorView.getVisibility() == Component.VISIBLE);
    }

    @Test
    public void emoticonsIndicatorViewPlayBy() {
        EmoticonsIndicatorView emoticonsIndicatorView = new EmoticonsIndicatorView(context, attrSet);
        PageSetEntity pageSetEntity = new PageSetEntity.Builder()
                .addPageEntity(new PageEntity(new SimpleAppsGridView(context)))
                .setShowIndicator(true)
                .build();
        int startPosition = -1;
        int nextPosition = -1;
        emoticonsIndicatorView.playBy(startPosition, nextPosition, pageSetEntity);
        assertTrue("It's right", emoticonsIndicatorView.getVisibility() == Component.VISIBLE);
    }

    @Test
    public void emoticonsKeyboardUtilsGetFontHeight() {
        Text text = new Text(context);
        text.setText("text");
        text.setTextSize(16);
        int height1 = EmoticonsKeyboardUtils.getFontHeight(text);
        text.setTextSize(18);
        int height2 = EmoticonsKeyboardUtils.getFontHeight(text);
        assertTrue("It's right", height1 < height2);
    }

    @Test
    public void imageLoaderDisplayImage() {
        ImageLoader imageLoader = ImageLoader.getInstance(context);
        String assetsUriStr = "assets://xhsemoji_19.png";
        Image assetsImage = new Image(context);
        imageLoader.displayImage(assetsUriStr, assetsImage);
        assertNotNull("It's right", assetsImage.getPixelMap().getImageInfo());
        String drawableUriStr = "drawable://icon_emoji";
        Image drawableImage = new Image(context);
        imageLoader.displayImage(drawableUriStr, drawableImage);
        assertNotNull("It's right", drawableImage.getPixelMap().getImageInfo());
    }

    @Test
    public void emoticonsEditTextOnBackKeyClick() {
        EmoticonsEditText emoticonsEditText = new EmoticonsEditText(context, attrSet);
        EmoticonsEditText.OnBackKeyClickListener listener = new EmoticonsEditText.OnBackKeyClickListener() {
            @Override
            public void onBackKeyClick() {
                assertTrue("It's right", true);
            }
        };
        emoticonsEditText.setOnBackKeyClickListener(listener);
        listener.onBackKeyClick();
    }

    @Test
    public void emoticonsEditTextSetOnSizeChanged() {
        EmoticonsEditText emoticonsEditText = new EmoticonsEditText(context, attrSet);
        EmoticonsEditText.OnSizeChangedListener listener = new EmoticonsEditText.OnSizeChangedListener() {
            @Override
            public void onSizeChanged(int w, int h, int oldw, int oldh) {
                assertTrue("It's right", w == 1 & h == 1);
            }
        };
        emoticonsEditText.setOnSizeChangedListener(listener);
        listener.onSizeChanged(1, 1, 0, 0);
    }

    @Test
    public void emoticonPageViewSetNumColumns() {
        EmoticonPageView emoticonPageView = new EmoticonPageView(context, attrSet);
        emoticonPageView.setNumColumns(1);
        ListContainer gvEmotion = emoticonPageView.getEmoticonsGridView();
        TableLayoutManager tableLayout = (TableLayoutManager) gvEmotion.getLayoutManager();
        assertTrue("It's right", 1 == tableLayout.getColumnCount());
    }

}