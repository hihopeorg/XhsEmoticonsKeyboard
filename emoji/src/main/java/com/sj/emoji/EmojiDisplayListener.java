package com.sj.emoji;

import ohos.app.Context;

/**
 * Created by sj on 16/3/22.
 */
//todo Span相关
public interface EmojiDisplayListener {

    void onEmojiDisplay(Context context, String spannable, String emojiHex, int fontSize, int start, int end);

//	void onEmojiDisplay(Context context, Spannable spannable, String emojiHex, int fontSize, int start, int end);
}
